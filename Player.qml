import QtQuick 2.4

import KMode 1.0
import KPlr 1.0
import Logger 1.0

PlayerForm {    
    Logger{
        id: logPlr
    }

    states:[
        State{
            name: "NoPartner"
            PropertyChanges {
                target: btnAddModeToAll
                visible: true
            }
            PropertyChanges {
                target: comboPartner
                visible: false
            }
            PropertyChanges {
                target: labelPartner
                visible: false
            }
        }
    ]

    Connections{
        target: btnAddModeToAll
        onClicked:{
            if( tableModes.currentRow === -1 || !checkSure.checked )return
            checkSure.checked = false
            for( var i = 0; i < tablePlrModel.count; i++ ){
                tablePlr.currentRow = i
                btnAddPlr.clicked()
            }
        }
    }

    Connections{
        target: btnAddPlr
        onClicked:{
            if( tableModes.currentRow === -1 || tablePlr.currentRow === -1 )return
            if( comboPartner.enabled)
                kitt.getMode( modelModes.get( tableModes.currentRow ).mode ).addPlr( kitt.getPlr( tablePlrModel.get( tablePlr.currentRow).name ),
                                                                                    kitt.getPlr( comboPartner.currentText ))
            else
                kitt.getMode( modelModes.get( tableModes.currentRow ).mode ).addPlr( kitt.getPlr( tablePlrModel.get( tablePlr.currentRow).name ))
        }
    }

    Connections{
        target: btnCreatePlr
        onClicked: editPlr.accepted()
    }

    Connections{
        target: btnKillPlr
        onClicked:{
            if( !checkSure.checked )return

            kitt.deletePlr( editPlr.text )
            checkSure.checked = false
        }
    }

    Connections{
        target: editPlr
        onAccepted:{
            kitt.newPlr( editPlr.text )
            editPlr.text = ""
        }
    }

    Connections{
        target: kitt
        onModeAdded: modelModes.append({ mode: mode.name })
        onModeDeleted:{
            for( var i = 0; i < modelModes.count; i++ )
                if( modelModes.get( i ).mode === strName ){
                    modelModes.remove( i )
                    removeMode( strName, -1 )
                    return
                }
        }
        onPlrAdded:{
            tablePlrModel.append({ name: plr.name })
            sortTable()
        }
        onPlrAddedToMode:{
            var pid = getPlrIndex( plr )
            if( pid === -1 )return
            var modes
            if( tablePlrModel.get( pid ).modes )
                modes = mode.name + " " + tablePlrModel.get( pid ).modes
            else
                modes = mode.name
            tablePlrModel.setProperty( pid, "modes", modes )
        }
        onPlrRemovedFromMode:{
            var index = getPlrIndex( plr )
            if( index !== -1 )removeMode( mode.name, index )
        }
        onPlrKilled:{
            for( var i = 0; i < tablePlrModel.count; i++ )
                if( tablePlrModel.get( i ).name === strName ){
                    tablePlrModel.remove( i )
                    return
                }
        }
    }

    Connections{
        target: tableModes
        onCurrentRowChanged:{
            var mode = kitt.getMode( modelModes.get( tableModes.currentRow ).mode)
            switch( mode.mode ){
            case ( KMode.MODE_PRELIM | KMode.MODE_DOUBLE ):
                state = "*"
                break;
            default:
                state = "NoPartner"
            }
        }
    }

    Connections{
        target: tablePlr
        onActivated: btnAddPlr.clicked()
        onCurrentRowChanged: editPlr.text = tablePlrModel.get( tablePlr.currentRow ).name
    }

    Keys.onEnterPressed: btnAddPlr.clicked()
    Keys.onSpacePressed: btnAddPlr.clicked()

    Component.onCompleted:{
        logPlr.log( "Plr completed ")
        kitt.loadPlrBase( "kitt.plr" )
    }

    function getPlrIndex( plr ){
        for( var i = 0; i < tablePlrModel.count; i++ )
            if( plr.name === tablePlrModel.get( i ).name )return i
        return -1
    }

    function removeMode( strMode, index ){
        for( var i = ( index === -1 ? 0 : index ); i < ( index === -1 ? tablePlrModel.count : index + 1 ); i++ ){
            var str = tablePlrModel.get( i ).modes
            if( str ){
                var beg = str.indexOf( strMode )

                if( beg !== -1 ){
                    var res = str.substr( 0, beg )
                    res += str.substr( beg + strMode.length + 1, str.length - res.length - strMode.length )
                    tablePlrModel.setProperty( i, "modes", res )
                }
            }
        }
    }

    function sortTable(){
        var i = tablePlrModel.count - 1
        var dummy
        while( i > 0 ){
            if( tablePlrModel.get( i ).name < tablePlrModel.get( i - 1 ).name ) tablePlrModel.move( i, i-1, 1 )
            else break

            i--
        }
    }
}

