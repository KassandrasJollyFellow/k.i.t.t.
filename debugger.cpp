#include "debugger.h"

/*!
 * \brief Standard constructor.
 * \param parent Set to a valid QObject pointer to make this Debugger object known to the Qt framework.
 * \details Creates a new RemoteKITT object. Connects it with this Debugger object and transfers it to a new thread.
 */
Debugger::Debugger(QObject *parent) : QObject(parent)
{
    RemoteKITT* prk = new RemoteKITT;
    prk->moveToThread( &m_thread );
    connect( &m_thread, &QThread::finished, prk, &QObject::deleteLater );
    connect( this, &Debugger::msgOut, prk, &RemoteKITT::msgIn );
    connect( prk, &RemoteKITT::msgOut, this, &Debugger::msgIn );
    m_thread.start();
}

/*!
 * \brief Standard deconstructor
 * \details Also kills thread and with it the RemoteKITT object.
 */
Debugger::~Debugger()
{
    m_thread.quit();
    m_thread.wait();
}
/*!
 * \brief Interface where messages from and for the RemoteKITT object are received.
 * \param msg RemoteKITT::Msg value.
 * \param var Value depends on _msg_. See RemoteKITT::Msg for details.
 * \details Currently supported values for _msg_ are:
 * - RemoteKITT::RKM_FINISHED
 * Emits log( "finished" ).
 * - RemoteKITT::RKM_LOG
 * Emits log( _var_ ).
 * - RemoteKITT::RKM_PROGRESS
 * Emits progress( _var_ ).
 * - Other values are forwarded emitting msgOut( _msg_, _var_ ).
 */
void Debugger::msgIn( const int &msg, const QVariant &var ){
    switch( msg ){
    case RemoteKITT::RKM_LOG:{
        log( var );
        break;
    }
    case RemoteKITT::RKM_FINISHED:{
        log( "finished" );
        break;
    }
    case RemoteKITT::RKM_PROGRESS:{
        progress( var );
        break;
    }
    default: msgOut( msg, var );
    }
}

