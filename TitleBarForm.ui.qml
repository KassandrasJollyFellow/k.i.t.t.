import QtQuick 2.4
import QtQuick.Controls 1.2
import QtQuick.Layouts 1.0

Item {
    id: titleBar
    Layout.fillWidth: true

    RowLayout{
        id: rowMain
        anchors.fill: parent
        Rectangle{
            id: rDrag
            color: "black"
            Layout.fillHeight: true
            Layout.fillWidth: true
            opacity: 0.5
            radius: 10
            MouseArea{
                id: maMain
                anchors.fill: parent
                hoverEnabled: true
                acceptedButtons: Qt.LeftButton | Qt.RightButton
            }
            AnimatedImage{
                anchors.fill: parent
                opacity: 0.3
                source: "kitt.gif"
            }
            Text{
                anchors.horizontalCenter: parent.horizontalCenter
                text: "K.I.T.T. 0.2"
                color: "white"
            }
        }
        RowLayout{
            spacing: 10
            Label{
                id: labelClock
                color: "white"
                font.bold: true
                font.pointSize: 13
            }
            Label {
                id: labelMin
                color: "#ffffff"
                font.bold: true
                scale: 1.2
                text: qsTr("_")
                MouseArea{ id: maMin; hoverEnabled: true; anchors.fill: parent }
            }
            Label {
                id: labelMax
                color: "#ffffff"
                text: qsTr("  ")
                font.bold: true
                font.strikeout: true
                scale: 1.4
                MouseArea{ id: maMax; hoverEnabled: true; anchors.fill: parent }
            }
            RowLayout{
                CheckBox {
                    id: checkCls4Sure
                    text: "<font color=\"red\">schließen ?</font>"
                    visible: false
                }
                Label {
                    id: labelCls
                    color: "white"
                    font.bold: true
                    text: qsTr("X")
                    MouseArea{ id: maCls4Sure; hoverEnabled: true; anchors.fill: parent }
                }
                MouseArea{ id: maCls; hoverEnabled: true; anchors.fill: parent; acceptedButtons: "NoButton" }
            }
        }
    }

    property alias checkCls4Sure: checkCls4Sure
    property alias labelClock: labelClock
    property alias labelCls: labelCls
    property alias labelMax: labelMax
    property alias labelMin: labelMin
    property alias maCls: maCls
    property alias maCls4Sure: maCls4Sure
    property alias maMain: maMain
    property alias maMax: maMax
    property alias maMin: maMin
    property alias rDrag: rDrag
}

