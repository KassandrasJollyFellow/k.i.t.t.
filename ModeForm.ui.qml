import QtQuick 2.4
import QtQuick.Controls 1.2
import QtQuick.Layouts 1.0

Item {
    id: item1
    width: 800
    height: 600

    Rectangle {
        id: rectangle1
        color: "#000000"
        border.width: 0
        anchors.fill: parent
    }

    ColumnLayout {
        id: colMain
        anchors{ fill: parent; margins: 10 }
        spacing: 10

        RowLayout {
            spacing: 10
            TextField {
                id: editTable
                placeholderText: qsTr("Tisch")
            }
            Button{
                id: btnAddTable
                text: qsTr( "Tisch hinzufügen" )
            }
            Button {
                id: btnKillTable
                text: qsTr("Tisch löschen")
            }
        }
        TableView{
            id: tableTables
            Layout.fillHeight: true
            Layout.fillWidth: true
            model: ListModel{
                id: modelTables
            }
            TableViewColumn{
                title: "Tisch"
                role: "table"
            }
        }
        ModeCtrls {
            id: modeCtrls
            Layout.fillWidth: true
        }
    }

    property alias btnAddTable: btnAddTable
    property alias btnKillTable: btnKillTable    
    property alias editTable: editTable
    property alias tableTables: tableTables
    property alias modelTables: modelTables
}

