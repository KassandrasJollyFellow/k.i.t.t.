#ifndef RANKS_H
#define RANKS_H

#include <QList>
#include <QObject>

#include "match.h"
#include "ranking.h"

class Ranks : public QObject
{
    Q_OBJECT
public:
    explicit Ranks(QObject *parent = 0);
    ~Ranks();

    inline Ranking* operator[]( int index ){ return m_lRankings.at( index );}

signals:
    void rankingAdded( Ranking* ranking );
    void rankingRemoved( Player* plr );
public slots:
    inline int count(){ return m_lRankings.count();}
    inline QList< Ranking* >::iterator begin(){ return m_lRankings.begin();}
    inline QList< Ranking* >::iterator end(){ return m_lRankings.end();}
    inline Ranking* find( QString strName ){ foreach( Ranking* pr, m_lRankings )if( pr->player()->name() == strName )return pr;return NULL;}
    inline Ranking* get( int index ){ return ( *this )[ index ];}

    int addRanking( Player* plr );

    void clear();    
private:
    QList< Ranking* > m_lRankings;
};

#endif // RANKS_H
