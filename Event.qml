import QtQuick 2.4
import QtQuick.Dialogs 1.2

import KMode 1.0
import KPlr 1.0

import "qrc:/Event"

EventForm {
    property KMode km

    MessageDialog{
        id: msgReroll
        title: "Es gibt ähnliche Paarungen im Vergleich zur letzten Runde"
        text: "Erneut auslosen?"
        standardButtons: StandardButton.Yes | StandardButton.No
    }

    Connections{
        target: km
        onAddedSet:{
            var match
            for( var i = 0; i < modelGames.count; i++ ){
                match = modelGames.get( i )
                if( match.id === idMatch ){
                    match.result = getResTxt( iBalls )
                    return
                }
            }
        }

        onMatchAdded:{
            if( km.mode & KMode.MODE_DYP ){
                if( !checkMatchFeelgood( match )){
                    msgReroll.open()
                }
            }

            if( km.mode & KMode.MODE_DOUBLE )
                modelGames.append({ round: mode.round, table: match.table, p11: match.getPlr( 0 ) ?
                                                   match.getPlr( 0 ).name : km.getSupportPlr( match.id ).name, p12: match.getPlr( 1 ) ?
                                                   match.getPlr( 1 ).name : km.getSupportPlr( match.id ).name, p21: match.getPlr( 2 ) ?
                                                   match.getPlr( 2 ).name : km.getSupportPlr( match.id ).name, p22: match.getPlr( 3 ) ?
                                                   match.getPlr( 3 ).name : km.getSupportPlr( match.id ).name,
                                                   result: match.getSetCount() ? getResTxt( match.getSet( 0 )) : "", id: match.id })
            else
                modelGames.append({ round: mode.round, table: match.table, p11: match.getPlr( 0 ).name, p21: match.getPlr( 2 ).name,
                                                   result: match.getSetCount() ? getResTxt( match.getSet( 0 )) : "", id: match.id })                        
            km.testForTablesToPlay()
        }
        onMatchRemoved:{
            for( var i = 0; i < modelGames.count; i++ )
                if( modelGames.get( i ).id === id ){
                    modelGames.remove( i )
                    return
                }
        }
        onTableRemoved:{
            for( var i = 0; i < modelGames.count; i++ )
                if( modelGames.get( i ).table === table ){
                    modelGames.setProperty( i, "table", "" )
                    return
                }
        }

        onTableSetToPlay:{
            for( var i = 0; i < modelGames.count; i++ )
                if( modelGames.get( i ).id === id ){
                    modelGames.setProperty( i, "table", table )
                }
        }
    }

    Connections{
        target: tableGames
        onActivated: ctrls.btnSetRes.clicked()
        onCurrentRowChanged:{
            if( modelGames.get( tableGames.currentRow ).result )
                ctrls.sliderBalls.value = km.getMatch( modelGames.get( tableGames.currentRow ).id ).getSet( 0 )
        }
    }
    Keys.onPressed:{
        switch( event.key ){
        case Qt.Key_Left:
            if( ctrls.sliderBalls.value > 0 ) ctrls.sliderBalls.value = ctrls.sliderBalls.value - 1
            break;
        case Qt.Key_Right:
            if( ctrls.sliderBalls.value < ctrls.sliderBalls.maximumValue ) ctrls.sliderBalls.value = ctrls.sliderBalls.value + 1
            break
        case Qt.Key_Space:
        case Qt.Key_Enter:
            ctrls.btnSetRes.clicked()
            break
        }
    }

    function checkMatchFeelgood( match ){
        return true
    }

    function getGoalsFirst( balls ){
        var v = balls
        if( v === km.balls / 2 ) return v
        if( v > km.balls / 2 ) return km.balls - balls
        return ( km.balls - km.balls % 2 ) / 2 + 1
    }

    function getGoalsSecond( balls ){
        var v = balls

        if( v <= km.balls / 2 ) return v
        return ( km.balls - km.balls % 2 ) / 2 + 1
    }

    function getResTxt( balls ){
        return getGoalsFirst( balls ) + " : " + getGoalsSecond( balls )
    }
}

