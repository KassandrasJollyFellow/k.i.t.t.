#include "remotekitt.h"

RemoteKITT::RemoteKITT(QObject *parent) : QObject(parent), m_iDbgPlrCount( 0 ), m_iDbgRoundCount( 0 ), m_iDbgTableCount ( 0 )
  ,m_pk( NULL )
{
}

RemoteKITT::~RemoteKITT()
{
    if( m_pk ){
        delete m_pk;
        m_pk = NULL;
    }
}

void RemoteKITT::debug( const QVariant &var ){
    int i;
    Mode mode;
    QList< QPair< unsigned int, QString >> luiModes = { QPair< unsigned int,QString >( Mode::MODE_SINGLE, "single" )
                                                        ,QPair< unsigned int,QString >( Mode::MODE_MDYP, "mdyp" )};

    msgOut( RKM_LOG, "debug" );

    init();

    msgOut( RKM_LOG, "erstelle Spieler");
    for( i = 0; i < m_iDbgPlrCount; i++ )msgIn( RKM_CREATEPLR, i );
    msgOut( RKM_LOG, m_pk->getPlrCount());

    msgOut( RKM_LOG, "erstelle Tische" );
    for( i = 0; i < m_iDbgTableCount; i++ )msgIn( RKM_CREATETABLE, i );
    msgOut( RKM_LOG, m_pk->getTableCount());

    msgOut( RKM_LOG, "erstelle Turniere" );
    mode.setBalls( m_iDbgBallCount );
    mode.setSets( m_iDbgSetCount );
    for( QList< QPair <unsigned int, QString >>::iterator it = luiModes.begin(); it != luiModes.end(); it++ ){
        msgOut( RKM_LOG, ( *it ).second );
        mode.setMode( ( *it ).first );
        mode.setName( ( *it ).second );
        m_pk->newMode( &mode );
    }
    msgOut( RKM_LOG, m_pk->getModeCount());

    msgOut( RKM_FINISHED, 0 );
}

void RemoteKITT::init(){
    msgOut( RKM_LOG, "init" );

    if( !m_pk )m_pk = new KITT;
    while( m_pk->getModeCount())m_pk->deleteMode( m_pk->getMode( 0 ));
    while( m_pk->getPlrCount())m_pk->deletePlr( m_pk->getPlr( 0 )->name());
    while( m_pk->getTableCount())m_pk->deleteTable( m_pk->getTable( 0 ));
}

void RemoteKITT::msgIn(const int &msg, const QVariant &var ){
    switch( msg ){
    case RKM_CREATEPLR:{
        m_pk->newPlr( var.toString());
        break;
    }
    case RKM_CREATETABLE:{
        m_pk->newTable( var.toString());
        break;
    }
    case RKM_DBG:{
        debug( var );
        break;
    }
    case RKM_DBG_SETBALL:{
        setDbgBallCount( var.toInt());
        break;
    }
    case RKM_DBG_SETPLR:{
        setDbgPlrCount( var.toInt());
        break;
    }
    case RKM_DBG_SETROUND:{
        setDbgRoundCount( var.toInt());
        break;
    }
    case RKM_DBG_SETSET:{
        setDbgSetCount( var.toInt());
        break;
    }
    case RKM_DBG_SETTABLE:{
        setDbgTableCount( var.toInt());
        break;
    }
    default: msgOut( RKM_UNKNOWN, 0);
    }
}

void RemoteKITT::setDbgBallCount(int iCount){
    if( iCount <= 0 || iCount == m_iDbgBallCount )return;

    m_iDbgBallCount = iCount;

    onDbgBallCountChanged();
}

void RemoteKITT::setDbgPlrCount( int iCount ){
    if( iCount <= 0 || iCount == m_iDbgPlrCount )return;

    m_iDbgPlrCount = iCount;

    onDbgPlrCountChanged();
}

void RemoteKITT::setDbgRoundCount( int iCount ){
    if( iCount <= 0 || iCount == m_iDbgRoundCount )return;

    m_iDbgRoundCount = iCount;

    onDbgRoundCountChanged();
}

void RemoteKITT::setDbgSetCount(int iCount){
    if( iCount <= 0 || iCount == m_iDbgSetCount )return;

    m_iDbgSetCount = iCount;

    onDbgSetCountChanged();
}

void RemoteKITT::setDbgTableCount( int iCount ){
    if( iCount <= 0 || iCount == m_iDbgTableCount )return;

    m_iDbgTableCount = iCount;

    onDbgTableCountChanged();
}

