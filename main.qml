import QtQuick 2.4
import QtQuick.Controls 1.3
import QtQuick.Window 2.2
import QtQuick.Dialogs 1.2

import KITT 1.0
import KPlr 1.0
import Logger 1.0

ApplicationWindow {
    id: aw
    height: 600
    width: 800
    title: qsTr("KITT")
    visible: true
    visibility: Qt.WindowFullScreen
    flags: Qt.FramelessWindowHint | Qt.WindowMinimizeButtonHint | Qt.TabFocus

    Item{
        id: pos
        x: 0
        y: 0
    }

    MainForm {
        id: mf
        anchors.fill: parent
    }

    KITT{
        id: kitt
    }     

    Logger{
        id: logMain
    }

    FileDialog{
        id: fileDlg
        nameFilters: [ " Events ( *.evt.xml *.evt.xml.tmp )" ]
        onAccepted: kitt.newMode( 0, fileDlg.fileUrl )
    }

    onActiveChanged:{
        logMain.log( "AW::active :" )
        logMain.log( active )
    }
    onClosing:{
        if( kitt.getPlrCount())kitt.savePlrBase( "kitt.plr" )
        if(kitt.getTableCount())kitt.saveTableBase( "kitt.tbl" )
        logMain.log( "AW::onClosing" )
    }
    onVisibilityChanged: {
        logMain.log( "AW::visible: ")
        logMain.log( visible )
    }
}
