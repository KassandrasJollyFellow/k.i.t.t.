#include "kitt.h"

/*!
 * \param parent Set to a valid QObject pointer to make this KITT object known to the Qt framework.
 * \brief Sets the parent of this object to parent .
 *
 * A call will be logged as block _KITT::KITT_
 */
KITT::KITT( QObject *parent ) : QObject( parent )
{
    m_log.begin( "KITT::KITT" );

    this->setParent( parent );

    m_log.end( "KITT::KITT" );
}

/*!
 * \brief Deletes all still existent Player and Mode objects.
 *
 * A call will be logged as block _KITT::~KITT_
 */
KITT::~KITT()
{
    m_log.begin( "KITT::~KITT" );

    foreach( Mode* pm, m_lMode )
        delete pm;
    foreach( Player* plr, m_lPlr )
        delete plr;

    m_log.end( "KITT::~KITT" );
}

/*!
 * \brief Creates a new Player object with the name _strName_.
 * A call will be logged as block _KITT::newPlr_
 * \param strName Name of the new player.
 * \return Pointer to the stored Player object.
 * _NULL_ if _strName_ is "".
 *
 * If a Player object with the name _strName_ already exists, no new Player object is created and a pointer to the existing object is returned.
 */
Player* KITT::newPlr( QString strName ){
    m_log.begin( "KITT::newPlr" );
    m_log.log( "name: '" + strName +"'" );

    if( strName == "" ){
        m_log.log( "invalid param" );
        m_log.end( "KITT::newPlr" );
        return NULL;}

    foreach( Player* plr, m_lPlr )
        if( plr->name() == strName ){
            m_log.log( "player already exists" );
            m_log.end( "KITT::newPlr" );
            return plr;}

    Player* plr = new Player( this );

    plr->setName( strName );
    m_lPlr.push_back( plr );

    plrAdded( plr );

    m_log.end( "KITT::newPlr" );
    return plr;
}
/*!
 * \brief Reads the mode of the event stored in file _url_.
 *
 * A call will be logged as block _KITT::getEventMode_
 * \param url File url to a valid KITT event file.
 * \return Value of the _event_ element read in file _url_.
 * If _url_ is not a valid KITT event file url, Mode::MODE_UNKNOWN is returned.
 */
unsigned int KITT::getEventMode( QUrl url )
{
    m_log.begin( "KITT::getEventMode" );
    m_log.log( "file: '" + url.fileName() + '"' );

    QFile file( url.toLocalFile());
    QXmlStreamReader sr( &file );

    if( file.open( QIODevice::ReadOnly )){
        sr.readNext();
        if( !sr.isStartDocument()) goto gem_end;
        sr.readNext();
        if( !sr.isStartElement()) goto gem_end;
        if( sr.name() != "event" )goto gem_end;
        sr.readNext();
        if( !sr.isCharacters()) goto gem_end;
        sr.readNext();
        if( !sr.isStartElement()) goto gem_end;
        sr.readNext();
        if( !sr.isCharacters()) goto gem_end;

        file.close();

        m_log.end( "KITT::getEventMode" );
        return sr.text().toUInt();
    }

gem_end:
    file.close();

    m_log.log( "unable to get event mode of '" + url.fileName() + "'" );
    m_log.end( "KITT::getEventMode" );
    return Mode::MODE_UNKNOWN;
}

/*!
 * \brief Deletes the Mode object with the adress _pm_.
 * \param pm
 * \details If _pm_ is _NULL_  or no Mode object with the adress _pm_ is found, nothing is done.
 *
 * Emits KITT::modeDeleted( QString _pm->name()_ ).
 *
 * A call will be logged as block KITT::deleteMode.
 */
void KITT::deleteMode( Mode* pm ){
    m_log.begin( "KITT::deleteMode" );

    if( !pm ){
        m_log.log( "pm == NULL" );
        m_log.end( "KITT::deleteMode" );
        return;
    }

    m_log.log( pm->name());

    Mode* pmDel = NULL;

    foreach( Mode* pmCur, m_lMode )
        if( *pmCur == pm ){
            pmDel = pmCur;
            break;
        }

    if( !pmDel ){
        m_log.log( "mode not found" );
        m_log.end( "KITT::deleteMode" );
        return;
    }

    QString strName = pmDel->name();

    m_lMode.removeOne( pmDel );
    delete pmDel;
    modeDeleted( strName );

    m_log.end( "KITT::deleteMode" );
}
/*!
 * \brief Deletes the Player object with the name _strName_
 * \param strName Name of player to delete.
 * \details If no Player object with the name _strName_ exists, nothing is done.
 *
 * Emits KITT::plrKilled( QString _strName_ ).
 *
 * A call will be logged as block KITT::deletePlr.
 */
void KITT::deletePlr( QString strName ){
    m_log.begin( "KITT::deletePlr" );
    m_log.log( "name: '" + strName + "'" );
    int i = 0;

    foreach( Player* plr, m_lPlr ){
        if( plr->name() == strName ){
            QString str = plr->name();
            delete plr;
            m_lPlr.removeAt( i );
            plrKilled( str );

            m_log.end( "KITT::deletePlr" );
            return;
        }

        i++;
    }

    m_log.log( "player not found" );
    m_log.end( "KITT::deletePlr" );
}
/*!
 * \brief Removes the table with the name _strTable_ from the KITT::m_slTables member.
 * \param strTable Name of table to remove.
 * \details If no table with the name strTable exists, nothing is done.
 *
 * Emits KITT::tableKilled( QString _strTable_ ).
 *
 * A call will be logged as KITT::deleteTable.
 */
void KITT::deleteTable( QString strTable ){
    m_log.begin( "KITT::deleteTable" );
    m_log.log( "table: '" + strTable + "'" );

    if( !m_slTables.contains( strTable )){
        m_log.log( "table doesn't exist" );
        m_log.end( "KITT::deleteTable" );
        return;
    }

    if( m_slTables.removeOne( strTable )) {
        foreach( Mode* mode, m_lMode ) mode->removeTable( strTable );
        tableKilled( strTable );
    }

    m_log.end( "KITT::deleteTable" );
}
/*!
 * \brief Adds Player objects with the Player::m_strName members set to _strPlr_ and _strPartner_ to the Mode object with adress _mode_.
 * \param mode Pointer to Mode object where the players will be added. If _mode_ is _NULL_ or "", nothing is done.
 * \param strPlr Name of the first player in the team to add. If _strPlr_ is _NULL_ or "", nothing is done.
 * \param strPartner Name of the second player in the team to add. If _strPartner_ is _NULL_ or "" no partner is set.
 * \details The parent of _mode_ can be different from _this_.
 *
 * If _strPlr_ or _strPartner_ doesn't exist, new Player objects with such names are created.
 *
 * A call will be logged as block KITT::loadPlr.
 * \warning No test whether the Mode object _mode_ supports partnership is done.
 */
void KITT::loadPlr( Mode* mode, QString strPlr, QString strPartner ){
    m_log.begin( "KITT::loadPlr" );

    if( !mode || strPlr == "" ){
        m_log.log( "mode == NULL" );
        m_log.end( "KITT::loadPlr" );
        return;}

    m_log.log( "mode: '" + mode->name() + "' player: '" + strPlr + "' partner: '" + strPartner + "'" );

    Player* plr = newPlr( strPlr );
    Player* partner = newPlr( strPartner );

    if( !mode->getPlr( strPlr )){
            mode->addPlr( plr, NULL );
    }else plr->setPartner( mode->name(), partner, true );

    m_log.end( "KITT::loadPlr" );
}
/*!
 * \brief Calls KITT::newPlr with every name stored in file _strFile_.
 * \param strFile Name of file with stored player names.
 * \details If _strFile_ can't be opened, nothing is done.
 *
 * A call will be logged as block KITT::loadPlrBase
 */
void KITT::loadPlrBase( QString strFile ){
    m_log.begin( "KITT::loadPlrBase" );
    m_log.log( "file: '" + strFile + "'" );

    QFile f( strFile );
    QDataStream ds( &f );
    QString strName;

    if( !f.open( QIODevice::ReadOnly )){
        m_log.log( "can't open file");
        m_log.end( "KITT::loadPlrBase" );
        return;}

    do{
        ds >> strName;
        newPlr( strName );
    }while( strName != "" );

    f.close();

    m_log.end( "KITT::loadPlrBase" );
}

/*!
 * \brief Calls KITT::newTable with every name stored in file _strFile_.
 * \param strFile Name of the file with stored table names.
 * \details If _strFile_ can't be opened, nothing is done.
 *
 * A call will be logged as block KITT::loadTableBase
 */
void KITT::loadTableBase( QString strFile ){
    m_log.begin( "KITT::loadTableBase" );
    m_log.log( "file: '" + strFile + "'" );

    QFile f( strFile );
    QDataStream ds( &f );
    QString strTable;

    if( !f.open( QIODevice::ReadOnly )){
        m_log.log( "can't open file" );
        m_log.end( "KITT::loadTableBase" );
        return;}

    do{
        ds >> strTable;
        if( strTable != "" ) newTable( strTable );
    }while( strTable != "" );

    f.close();
    m_log.end( "KITT::loadTableBase" );
}
/*!
 * \brief Creates a new Mode object.
 * \param pm Pointer to Mode object describing the Mode object to create. If _pm_ is _NULL_, _urlFile_ is used instead.
 * \param urlFile Url to file with stored KITT event. If _pm_ is not _NULL_, _urlFile_ is ignored.
 * \details If _pm_ is _NULL_ and _urlFile_ is no valid KITT event file, nothing is done.
 * If _urlFile is used and the stored event element is unknown, a dummy Mode object will be created, but might not work properly.
 * See Mode::ModeType for details.
 *
 * Emits KITT::modeAdded( Mode* _< pointer to new Mode object >_ ).
 *
 * A call will be logged as block KITT::newMode.
 */
void KITT::newMode(Mode* pm , QUrl urlFile){
    m_log.begin( "KITT::newMode" );
    m_log.log( "pm = '" +  ( pm == NULL ? "NULL" : pm->name()  ) + "' + urlFile = '" + urlFile.fileName() + "'" );

    if(( pm && !pm->name().length()) && !urlFile.isLocalFile()){
        m_log.log( "invalid params");
        m_log.end( "KITT::newMode" );
        return;}

    Mode* pmNew;

    foreach( Mode* pmCur, m_lMode )
        if( *pmCur == pm ){
            m_log.log( "mode already exists" );
            m_log.end( "KITT::newMode" );
            return;}

    if( pm ){
        pmNew = new Prelim( this );

        pmNew->setMode( pm->mode());
        pmNew->setName( pm->name());
        pmNew->setBalls( pm->balls());
        pmNew->setSets( pm->sets());

        QObject::connect( pmNew, SIGNAL( loadedPlr( Mode*, QString, QString )), this, SLOT( loadPlr( Mode*, QString, QString )));
        QObject::connect( pmNew, SIGNAL( plrAdded( Player*, Mode* )), this, SIGNAL( plrAddedToMode( Player*,Mode* )));
        QObject::connect( pmNew, SIGNAL( plrRemoved(Player*,Mode* )), this, SIGNAL( plrRemovedFromMode(Player*,Mode* )));
    }else{
        unsigned int uiMode = getEventMode( urlFile );
        switch( uiMode ){
        case Mode::MODE_MDYP:
        case Mode::MODE_PRELIM | Mode::MODE_SINGLE:
        case Mode::MODE_PRELIM | Mode::MODE_DOUBLE:
        case Mode::MODE_PRELIM | Mode::MODE_DOUBLE | Mode::MODE_DYP:{
            pmNew = new Prelim( this );
            pmNew->setMode( uiMode );
            break;
        }
        default:{
            pmNew = new Mode( this );
            pmNew->setMode( uiMode );
        }
        }

        QObject::connect( pmNew, SIGNAL( loadedPlr( Mode*, QString, QString )), this, SLOT( loadPlr( Mode*, QString, QString )));
        QObject::connect( pmNew, SIGNAL( plrAdded( Player*, Mode* )), this, SIGNAL( plrAddedToMode( Player*,Mode* )));
        QObject::connect( pmNew, SIGNAL( plrRemoved(Player*,Mode* )), this, SIGNAL( plrRemovedFromMode(Player*,Mode* )));

        if( !pmNew->loadEvent( urlFile.toLocalFile())){
            delete pmNew;

            m_log.log( "error in event file" );
            m_log.end( "KITT::newMode" );
            return;
        }
    }

    m_lMode.append( pmNew );
    modeAdded( pmNew );

    if( !pm )pmNew->signalMatches();

    m_log.end( "KITT::newMode" );
}
/*!
 * \brief Adds a new table with the name _strTable_ to this KITT object.
 * \param strTable Name of the new table.
 * \details If _strTable_ is _NULL_ or _strTable_ is "" or a table with the name _strTable already exists, nothing is done.
 *
 * Emits KITT::tableAdded( _strTable_ ).
 *
 * A call will be logged as block KITT::newTable.
 */
void KITT::newTable( QString strTable ){
    m_log.begin( "KITT::newTable" );
    m_log.log( "strTable = '" + strTable + "'" );

    if( strTable == "" ){
        m_log.log( "invalid param" );
        m_log.end( "KITT::newTable" );
        return;}

    foreach( QString str, m_slTables)
        if( str == strTable ){
            m_log.log( "table already exists" );
            m_log.end( "KITT::newTable" );
            return;}

    m_slTables.append( strTable );

    tableAdded( strTable );

    m_log.end( "KITT::newTable" );
}

/*!
 * \brief Stores names of all existing Player objects in file _strFile_.
 * \param strFile Name of the save file.
 * \details If _strFile_ can't be opened, nothing is done.
 *
 * A call will be logged as block KITT::savePlrBase.
 */
void KITT::savePlrBase( QString strFile ){
    m_log.begin( "KITT::savePlrBase" );
    m_log.log( "file: '" + strFile + "'" );

    QFile f( strFile );
    QDataStream ds( &f );

    if( !f.open( QIODevice::WriteOnly )){
        m_log.log( "can't open file" );
        m_log.end( "KITT::savePlrBase" );
        return;}

    foreach( Player* plr, m_lPlr ){
        m_log.log( "stored player '" + plr->name() + "'" );
        ds << plr->name();}

    f.close();

    m_log.end( "KITT::savePlrBase" );
}
/*!
 * \brief Stores names of all existing tables in file _strFile_.
 * \param strFile Name of the save file.
 * \details If _strFile_ can't be opened, nothing is done.
 *
 * A call will be logged as block KITT::saveTableBase.
 */
void KITT::saveTableBase( QString strFile ){
    m_log.begin( "KITT::saveTableBase" );
    m_log.log( "file: '" + strFile + "'" );

    QFile f( strFile );
    QDataStream ds( &f );

    if( !f.open( QIODevice::WriteOnly )){
        m_log.log( "can't open file" );
        m_log.end( "KITT::saveTableBase" );
        return;}

    foreach( QString str, m_slTables ){
        m_log.log( "stored table '" + str + "'" );
        ds << str;}

    f.close();

    m_log.end( "KITT::saveTableBase" );
}
