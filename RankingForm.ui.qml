import QtQuick 2.4
import QtQuick.Layouts 1.0
import QtQuick.Controls 1.2

import "qrc:/Ranking"

Item {
    id: formRanking
    width: 400
    height: 400

    Rectangle {
        id: rectangle1
        color: "#000000"
        border.width: 0
        anchors.fill: parent
    }

        ColumnLayout {
            id: colMain
            anchors.rightMargin: 10
            anchors.leftMargin: 10
            anchors.bottomMargin: 10
            anchors.topMargin: 10
            anchors.fill: parent
            spacing: 10

            TableView{
                id: tableModes
                anchors.left: parent.left
                anchors.right: parent.right
                implicitHeight: 100
                model: ListModel{ id: modelModes }
                TableViewColumn{
                    role: "mode"
                    title: "Modus"
                }
            }

            TableView{
                id: tableRanking
                anchors.left: parent.left
                anchors.right: parent.right
                itemDelegate: TRankingDelegate{}
                Layout.fillHeight: true
                model: ListModel{ id: modelRankings }
                TableViewColumn{
                    role: "rank"
                    title: "Rang"
                }
                TableViewColumn{
                    role: "player"
                    title: "Spieler"
                }
                TableViewColumn{
                    role: "result"
                    title: "Spiele / Siege / Unentschieden / Niederlagen "
                }
                TableViewColumn{
                    role: "points"
                    title: "Punkte"
                }
                TableViewColumn{
                    role: "buchholz"
                    title: "Buchholzzahl"
                }
                TableViewColumn{
                    role: "sb"
                    title: "Sonneborn-Berger"
                }
            }
        }

        property alias modelModes: modelModes
        property alias modelRankings: modelRankings
        property alias tableModes: tableModes
    property alias tableRanking: tableRanking
}

