#include "player.h"

/*!
 * \brief Standard constructor.
 * \param parent Set to valid QObject object to make this TeamUp object known to the Qt framework.
 */
TeamUp::TeamUp( QObject* parent ): QObject(parent)
{

}
/*! \brief Standard deconstructor.*/
TeamUp::~TeamUp(){

}
/*!
 * \brief Sets the name of the event mode of this TeamUp object.
 * \param strMode New name of the event for this TeamUp obejct.
 * \details If _strMode_ is "" or _strMode_ equals the TeamUp::m_strMode member, nothing is done.
 * 
 * Emits TeamUp::onModeChanged.
 */
void TeamUp::setMode( QString strMode ){
    if( strMode == "" || strMode == m_strMode )return;

    m_strMode = strMode;

    onModeChanged();
}
/*!
 * \brief Sets the partner of the player this TeamUp object belongs to.
 * \param plr Pointer to Player object that is the new partner.
 * \details If _plr_ equals the TeamUp::m_plr member, nothing is done.
 * 
 * Emits TeamUp::onPlayerChanged.
 */
void TeamUp::setPlayer( Player* plr ){
    if( *plr == m_plr )return;

    m_plr = plr;

    onPlayerChanged();
}
/*!
 * \brief Standard constructor.
 * \param parent Set to a valid QObject object to make this Player object known to the Qt framework.
 */
Player::Player(QObject *parent) : QObject(parent), m_bActive( false ), m_bIsPlaying( false ), m_iByes( 0 ), m_strName( "" )
{
}
/*! \brief Standard deconstructor
 *  \details Deletes all remaining TeamUp objects in this Player object.
 */
//TODO: remove player deconstructed here from TeamUp objects of their respective partners.
Player::~Player()
{
    foreach( TeamUp* tu, m_teamUps )
        delete tu;
}
/*!
 * \brief Tests if two Player objects represent the same player.
 * \param plr Pointer to Player object to compare against this Player object.
 * \return _True_ if the Player::m_strName member of this and the _plr_ Player object are equal, otherwise _false_.
 */
bool Player::operator==( Player* plr ){
    if( plr->name() != m_strName )return false;

    return true;
}
/*!
 * \brief Player::partner
 * \param strMode
 * \return 
 */
Player* Player::partner( QString strMode ){
    if( !strMode.length())return NULL;

    foreach( TeamUp* tu, m_teamUps )
        if( tu->mode() == strMode )
            return tu->player();

    return NULL;
}

QQmlListProperty< TeamUp > Player::teamUps(){
    return QQmlListProperty< TeamUp >( this, m_teamUps );
}

TeamUp* Player::teamUp( int index ) const{
    return m_teamUps.at( index );
}

void Player::setActive( bool bActive ){
    if( bActive == m_bActive )return;

    m_bActive = bActive;

    onActiveChanged();
}

void Player::setByes( int iByes ){
    if( iByes < 0 || iByes == m_iByes )return;

    m_iByes = iByes;

    onByesChanged();
}

void Player::setName( QString strName ){
    if( strName == "" || strName == m_strName )return;

    m_strName = strName;

    onNameChanged();
}

void Player::setIsPlaying( bool bIsPlaying ){
    if( bIsPlaying == m_bIsPlaying )return;

    m_bIsPlaying = bIsPlaying;

    onIsPlayingChanged();
}

void Player::setPartner(QString strMode, Player* partner , bool bPartnerSet){
    if( !strMode.length() || !partner )return;
    bool bFound = false;

    foreach( TeamUp* tu, m_teamUps )
        if( tu->mode() == strMode ){
            tu->setPlayer( partner );
            bFound = true;
            break;
        }

    if( !bFound ){
        TeamUp* tu = new TeamUp( this );
        tu->setMode( strMode );
        tu->setPlayer( partner );
        m_teamUps.append( tu );
    }

    if( !bPartnerSet )partner->setPartner( strMode, this, true );
}
