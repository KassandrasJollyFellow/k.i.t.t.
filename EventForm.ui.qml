import QtQuick 2.4
import QtQuick.Controls 1.2
import QtQuick.Layouts 1.1

import "qrc:/Event"
import "qrc:/Mode"

Item {    
    anchors.fill: parent
    Rectangle {
        id: rectangle1
        color: "#000000"
        border.width: 0
        anchors.fill: parent
    }

    ColumnLayout{
        anchors.fill: parent
        anchors.topMargin: 10
        anchors.leftMargin: 10
        anchors.rightMargin: 10
        anchors.bottomMargin: 10

        spacing: 10
        TableChecks{ id: checksTable }
        EventCtrls{ id: ctrls }

        TableView{
            id: tableGames            
            itemDelegate: TModeDelegate{}

            Keys.forwardTo: [ Qt.Key_Left, Qt.Key_Right, Qt.Key_Space ]
            Layout.fillHeight: true
            Layout.fillWidth: true
            model: ListModel{ id: modelGames }
            rowDelegate: EventRowDelegate{}
            TableViewColumn{
                title: "Runde"
                role: "round"
                width: 50
            }
            TableViewColumn{
                title: "Tisch"
                role: "table"
            }
            TableViewColumn{
                title: "Team 1 Spieler 1"
                role: "p11"
            }
            TableViewColumn{
                title: "Team 1 Spieler 2"
                role: "p12"
            }
            TableViewColumn{
                title: "Team 2 Spieler 1"
                role: "p21"
            }
            TableViewColumn{
                title: "Team 2 Spieler 2"
                role: "p22"
            }
            TableViewColumn{
                title: "Ergebnis"
                role: "result"
            }
        }
    }

    property alias checksTable: checksTable
    property alias ctrls: ctrls
    property alias modelGames: modelGames
    property alias tableGames: tableGames
}

