import QtQuick 2.4

import KITT 1.0

import "qrc:/Event"

TourneyForm {
    Connections{
        target: kitt
        onModeAdded:{
            var v = Qt.createComponent( "Event.qml" )
            tabEvents.addTab( mode.name )
            v.createObject( tabEvents.getTab( tabEvents.count - 1 ), { km: mode })
            tabMain.currentIndex = 0
            tabEvents.currentIndex = tabEvents.count - 1
        }
        onModeDeleted:{
            for( var i = 0; i < tabEvents.count; i++ )
                if( tabEvents.getTab( i ).title === strName ){
                    tabEvents.removeTab( i )
                    if( tabEvents.count )tabEvents.currentIndex = tabEvents.count - 1
                    else tabMain.currentIndex = 1
                    return
                }
        }
    }
}

