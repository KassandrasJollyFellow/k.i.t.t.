#include "logger.h"

int Logger::m_iLoggerCount = 0;
int Logger::m_iOpenLoggerCount = 0;
QTextStream Logger::m_stream;
QFile Logger::m_file( "log.txt ");

/*!
 * \brief Standard constructor.
 * \param parent Set to a valid QObject object to make this Logger object known to the Qt framework.
 * \details If no log file name is set, Logger::m_file is set to "log.txt".
 * If the log file is not opened, it is opened. If the file exists, its contents are destroyed.
 *
 * Increases Logger::m_iLoggerCount and Logger::m_iOpenLoggerCount.
 */
Logger::Logger(QObject *parent) : QObject(parent), m_iID( m_iLoggerCount )
{
    if( !m_file.fileName().length())m_file.setFileName( "log.txt" );

    if( !m_file.isOpen())openFile();

    m_iLoggerCount++;
    m_iOpenLoggerCount++;
}

/*!
 * \brief Standard deconstructor.
 * \details Decreases Logger::m_iOpenLoggerCount and calls Logger::closeFile.
 */
Logger::~Logger()
{
    m_iOpenLoggerCount--;

    closeFile();
}
/*!
 * \brief Begins a new block in the log file.
 * \param str Name of the new block. The parameter _str_ of the corresponding call to Logger::end must equal this name.
 * \details If _str_ is _NULL_ or _str_ is "", nothing is done.
 *
 * Appends _str_ to Logger::m_lsBlocks and calls Logger::log with the argument "begin: '" + str + "'".
 */
void Logger::begin( QString str ){
    if( !str.length()) return;

    m_lsBlocks.append( str );
    log( "begin: '" + str + "'" );
}
/*!
 * \brief Ends a block in the log file.
 * \param str Name of the ending block. The parameter _str_ of the corresponding call to Logger::begin must equal this name.
 * \details If _str_ equals the last QString in Logger::m_lsBlocks, _str_ is removed from Logger::m_lsBlocks and Logger::log is
 * called with the argument "end: '" + str + "'".
 *
 * If _str_ __doesn't__ equal the last QString in Logger::m_lsBlocks, a matching block is searched and all blocks after the first matching
 * block are discarded. If no matching block begin exists, no block is discarded. In the first case a warning message will be logged to the
 * log file, in the second case an error message.
 */
void Logger::end( QString str ){
    if( str == m_lsBlocks.last()){
        log( "end: '" + str + "'" );
        m_lsBlocks.pop_back();
        return;
    }

    if( m_lsBlocks.contains( str )){
        log( "warning: reached end of block '" + str + "' missing end of blocks: ");
        while( true ){
            log( m_lsBlocks.back());
            m_lsBlocks.pop_back();
            if( str == m_lsBlocks.back()) return;
        }
    }else log( "error: reached end of block '" + str + "' without matching begin" );
}

/*!
 * \brief Removes this Logger object from the Logger counter and closes the log file if it was the last one. If the log file is not open,
 * nothing is done. A line stating "close" will be logged to the log file prior to closing.
 */
void Logger::closeFile(){
    if( !m_file.isOpen())return;

   log( "close" );

    if( !m_iOpenLoggerCount ) m_file.close();
}

/*!
 * \brief Writes a time stamp, the id of this Logger object and _str_ to the file stream.
 * \param str String to log.
 */
void Logger::log( QString str ){
    QString strLog;

    m_stream << "[ "  << time( NULL ) <<  "  ]" << m_iID << " : " << str << "\n";

}
/*!
 * \brief Opens the log file write only in text mode.
 */
void Logger::openFile(){
    m_file.open( QIODevice::Text | QIODevice::WriteOnly );
    m_stream.setDevice( &m_file );

    log( "started" );
}

/*!
 * \brief Changes the log file to _strFileName_ and calls Logger::openFile. If the file was open, Logger::closeFile is called before changing
 * the file name.
 * \param strFileName
 */
void Logger::setFileName( QString strFileName ){
    if( !strFileName.length() || strFileName == m_file.fileName())return;

    closeFile();

    m_file.setFileName( strFileName );

    openFile();
}

