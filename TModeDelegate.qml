import QtQuick 2.0

Item{    
    Rectangle{
        id: rBg
        anchors.fill: parent
        color: modelGames.get( styleData.row ).result ? "white" : "black"
        opacity: tableGames.currentRow === styleData.row ? 1.0 : 0.5
    }
    Text{
        anchors.fill: parent
        text: styleData.column === 0 ? modelGames.get( styleData.row ).round :
              styleData.column === 1 ? modelGames.get( styleData.row ).table :
              styleData.column === 2 ? modelGames.get( styleData.row ).p11 :
              styleData.column === 3 ? modelGames.get( styleData.row ).p12 :
              styleData.column === 4 ? modelGames.get( styleData.row ).p21 :
              styleData.column === 5 ? modelGames.get( styleData.row ).p22 : modelGames.get( styleData.row ).result
        font.pixelSize: parent.height - 10
        fontSizeMode: Text.VerticalFit
        font.bold:  true
        font.underline: tableGames.currentRow === styleData.row ? true : false
        color: modelGames.get( styleData.row ).result ? "black" : styleData.row === tableGames.currentRow ? "white" : "black"
    }
}

