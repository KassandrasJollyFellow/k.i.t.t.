#ifndef LOGGER_H
#define LOGGER_H

#include <QTextStream>
#include  <QFile>
#include <QObject>
#include <time.h>

/*!The Logger class is used to log actions of other objects to a single log file that is shared among the Logger objects.
 * It supports simple logging function with Logger::log and blocks structured with Logger::begin and Logger::end, validating nested Logger blocks.
 * Calls to Logger::begin are stored in a stack and are removed by matching calls to Logger::end. If a call to Logger::end doesn't match
 * the last stack element, a matching block begin is searched and all block begins after the first match are discarded and a warning
 * is logged to the log file. If no matching block begin is found, no other blocks are discarded and an error is logged to the log file.
 *
 * This class is registered int the QML framework as type _Logger_ version 1.0.
 */
class Logger : public QObject
{
    Q_OBJECT
    /*!QML property fileName*/
    Q_PROPERTY( QString fileName READ fileName WRITE setFileName NOTIFY onFileNameChanged)
public:
    explicit Logger(QObject *parent = 0);
    ~Logger();

signals:
    void onFileNameChanged(); /*!< Emitted when the log file has changed.*/
public slots:
    inline const QString fileName(){ return m_file.fileName();} /*!< Returns the file name of the log file.*/

    void begin( QString str );
    void end( QString str );
    void log( QString str );
    void setFileName( QString strFileName );
private:
    void closeFile();
    void openFile();

    int m_iID; /*!< Stores the unique id of this Logger object.*/
    QStringList m_lsBlocks; /*!< Stores the Logger blocks opened with Logger::begin.*/

    static int m_iLoggerCount; /*!< Keeps track of the number of Logger objects created to realize unique Logger ids.*/
    static int m_iOpenLoggerCount; /*!< Holds the number of the currently existing Logger objects.*/
    static QTextStream m_stream; /*!< Text stream managing write operations to the log file.*/
    static QFile m_file;/*!< File handle to the log file. Initially set to "log.txt"*/
};

#endif // LOGGER_H
