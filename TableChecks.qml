import QtQuick 2.0
import QtQuick.Controls 1.2

import KITT 1.0

Row{
    spacing: 10
    Repeater{
        id: repeater
        model: kitt.getTableCount()
        CheckBox{
            text: "<font color=\"white\">" + kitt.getTable( index ) + "</font>"
            onClicked:{
                if( checked ) km.addTable( kitt.getTable( index ))
                else km.removeTable( kitt.getTable( index ))
            }
        }
    }
    Connections{
        target: kitt
        onTableAdded: updateChecks()
        onTableKilled: updateChecks()
    }

    function updateChecks(){
        repeater.model = kitt.getTableCount()
        for( var i = 0; i < repeater.model; i++ )
            if( km.isTable( kitt.getTable( i )))
                repeater.itemAt( i ).checked = true
    }
}

