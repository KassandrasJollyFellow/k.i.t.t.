#include "mode.h"

/*!
 * \brief Returns the Player object with least byes from list _plplr_.
 * \param plplr List to search.
 * \return Pointer to player with least byes.
 * If multiple players with the same bye count exist, the first one found is returned.
 * If _plplr_ is _NULL_ or the number of Player objects stored in _plplr_ is 0, _NULL_ is returned.
 */
Player* leastByes(QList<Player *> *plplr ){
    if( !plplr || !plplr->count())return NULL;

    Player* plrCur = plplr->first();

    for( int i = 1; i < plplr->count(); i++ )
        if( plplr->at( i )->byes() < plrCur->byes())
            plrCur = plplr->at( i );

    return plrCur;
}
/*!
 * \brief Standard constructor.
 * \param parent Set to a valid QObject pointer to make this Mode object known to the Qt framework.
 * \details A call will be logged as Mode::Mode.
 */
Mode::Mode(QObject *parent) : QObject(parent)
{
    m_log.log( "Mode::Mode" );
}
/*!
 * \brief Standard deconstructor.
 * \details Calls Player::setIsPlaying( false ) for every player in every match this Mode object still contains.
 *
 * A call will be logged as block Mode::~Mode.
 */
//TODO: test if player can get stuck playing if called multiple times
Mode::~Mode()
{
    m_log.begin( "Mode::~Mode" );

     foreach( Match * match, m_lMatch )
        if( match->table().length())
            for( int i = 0; i < 4; i++ )
                if( match->getPlr( i ))
                    match->getPlr( i )->setIsPlaying( false );

     m_log.end( "Mode::~Mode" );
}
/*!
 * \brief Creates a new Match object specified by _pm_ and stores it in Mode::m_lMatch.
 * \param pm Pointer to a Match object reflecting the match to create.
 * \return _True_ if match was created and stored. Otherwise _false_.
 * \details If a new match was created and stored:
 * - Mode::save is called
 * - Mode::matchAdded( *new_mode*, _this_ ) is emitted
 *
 * If the match _pm_ already exists in this Mode object no new Match object is created or stored.
 *
 * A call will be logged as block Mode::addMatch.
 */
bool Mode::addMatch( Match* pm ){
    m_log.begin( "Mode::addMatch" );

    if( !pm ){
        m_log.log( "pm == NULL" );
        m_log.end( "Mode::addMatch" );
        return false;}

    foreach( Match* pmCur, m_lMatch )
        if( *pmCur == pm ){
            m_log.log( "match with same id already existing: '" + QVariant( pm->id()).toString() + "'" );
            m_log.end( "Mode::addMatch" );
            return false;}

    Match* pmNew = new Match( this );

    pmNew->setBalls( pm->balls());
    pmNew->setSets( pm->sets());
    pmNew->setTable( pm->table());

    for( int i = 0; i < 4; i++ )
        pmNew->setPlr( i, pm->getPlr( i ));

    for( int i = 0; i < pm->getSetCount(); i++ )
        pmNew->addSet( pm->getSet( i ));

    m_lMatch.append( pmNew );

    save();

    matchAdded( pmNew, this );

    m_log.end( "Mode::addMatch" );

    return true;
}
/*!
 * \brief Loads the match data stored in event file _strFile_.
 * \param strFile Name of the file to load.
 * \return _True_ if matches stored in _strFile_ were successfully read and added to this Mode object, otherwise _false_.
 * \details No other data of this Mode object than the Match objects are changed.
 *
 * For each player in _strFile_ a Mode::loadedPlr( _this_, *player_name*, *partner_name* ) signal is emitted.
 *
 * If _strFile_ can not be opened, nothing is done.
 * If an error occurs while reading match data, the state of Mode::m_lMatch is undefined.
 *
 * A call will be logged as block Mode::loadEvent.
 */
//TODO: add test if correct number of matches was read
bool Mode::loadEvent( QString strFile ){
    int iCount;
    Match match;
    QXmlStreamReader xml( &m_file );

    m_log.begin( "Mode::loadEvent" );

    m_file.setFileName( strFile );

    if( !m_file.open( QIODevice::ReadOnly | QIODevice::Text )){
        m_log.log( "can't open file: '" + strFile + "'" );
        m_log.end( "Mode::loadEvent" );
        return false;}

    while( xml.name() != "name" && xml.readNext() != QXmlStreamReader::Invalid );
    m_strName = getXmlVal( &xml ).toString();
    while( xml.name() != "balls" && xml.readNext() != QXmlStreamReader::Invalid );
    m_iBalls = getXmlVal( &xml ).toInt();
    while( xml.name() != "sets" && xml.readNext() != QXmlStreamReader::Invalid );
    m_iSets = getXmlVal( &xml ).toInt();
    while( xml.name() != "player" && xml.readNext() != QXmlStreamReader::Invalid );

    if( xml.hasError()){
        m_log.log( "error reading xml stream: " + xml.errorString());
        m_log.end( "Mode::loadEvent" );
        m_file.close();
        return false;
    }

    foreach( const QXmlStreamAttribute xa, xml.attributes())
        loadedPlr( this, xa.name().toString(), xa.value().toString());

    while( xml.name() != "matchCount" && xml.readNext() != QXmlStreamReader::Invalid );
    iCount = getXmlVal( &xml ).toInt();
    while( xml.name() != "matches" && xml.readNext() != QXmlStreamReader::Invalid );

    for( int i = 0; i < iCount; i++ ){
        while( xml.readNext() != QXmlStreamReader::Invalid && !xml.name().contains( "match" ));
        foreach( const QXmlStreamAttribute xa, xml.attributes()){
            if( xa.name() == "p11" ) match.setPlr( 0, getPlr( xa.value().toString()));
            else if( xa.name() == "p12" ) match.setPlr( 1, getPlr( xa.value().toString()));
            else if( xa.name() == "p21" ) match.setPlr( 2, getPlr( xa.value().toString()));
            else if( xa.name() == "p22" ) match.setPlr( 3, getPlr( xa.value().toString()));
            else if( xa.name() == "result" ) if( xa.value().toInt() != -1 )match.addSet( xa.value().toInt());
        }

        if( xml.hasError() ){
            m_log.log( "error reading xml stream: " + xml.errorString());
            m_log.end( "Mode::loadEvent" );
            m_file.close();
            return false;
        }else{
            addMatch( &match );
            match.removeSet( 0 );
            xml.readNext();}
    }

    m_file.close();

    m_log.end( "Mode::loadEvent" );

    return true;
}
/*!
 * \brief Returns true if this Mode object and _pm_ have the same Mode::m_strName member.
 * \param pm Mode object to test.
 * \return _True_ if the names of this mode and the mode _pm_ are identical, otherwise _false_.
 */
bool Mode::operator==( Mode* pm ){
    if( !pm ) return false;

    if( pm->name().contains( m_strName ) || m_strName.contains( pm->name()))return true;

    return false;
}
/*!
 * \brief Returns a random player of this Mode object that is not part of the match with id _idMatch_.
 * Used to provide random partner to single player in double event modes.
 * \param idMatch Id of match in question.
 * \return Returns pointer to selected player.
 * The player is part of this Mode object, but not participating in match with id _idMatch_.
 * \details A call is logged as Mode::getSupportPlr
 */
Player* Mode::getSupportPlr( int idMatch ){
    Player* plr;
    QList< Player* > lp =m_lPlr;

    foreach( Match* match, m_lMatch )
        if( match->id() == idMatch ){
            for( int i = 0; i < 4; i++ )
                lp.removeOne( match->getPlr( i ));

            break;
        }

    plr = lp.at( rand() % lp.count());

    m_log.log( "Mode::getSupportPlr( " + QVariant( idMatch ).toString() + " ) selected '" + plr->name() + "'" );

    return plr;
}
/*!
 * \brief Does nothing. Override in derived classes and use Mode::m_ranks to provide a ranking.
 * \return Returns _NULL_.
 */
Ranks* Mode::getRanks(){
    return NULL;
}
/*!
 * \brief Adds or deactivates players _plr_ sets his partner to _partner_ to / in this Mode object, depending on whether they are active.
 * \param plr Name of first player in team. If _NULL_ Mode::addPlr does nothing.
 * \param partner Name of second player in team. If _NULL_ no partner is set for player _plr_.
 * \details: Emits Mode::plrAdded( _plr_, _partner_ ). Emits Mode::plrRemoved( _partner_, this ).
 *
 * A call is logged as block Mode::addPlr.
 * \warning Depending on the Player::m_bActive member is bad behaviour and subject to change.
 */
//TODO: find solution to warning
void Mode::addPlr( Player* plr, Player* partner )
{
    m_log.begin( "Mode::addPlr" );

    if( !plr ){
        m_log.log( "plr == NULL" );
        m_log.end( "Mode::addPlr" );
        return;}

    foreach( Player* plrCur, m_lPlr )
        if( *plrCur == plr ){
            plrCur->setActive( !plrCur->active());
            if( plrCur->active()){
                plrAdded( plr, this );
                checkPartner( plr, partner );

                m_log.log( "player '" + plr->name() +"' set active" );
            }
            else{
                plrRemoved( plr, this );
                if( plr->partner( name())){
                    partner = plr->partner( name());
                    foreach( Player* plrCur, m_lPlr )
                        if( plrCur == plr ){
                            partner->setActive( false );
                            plrRemoved( partner, this );
                        }
                }

                m_log.log( "player '" + plr->name() +"' set inactive" );
            }

            m_log.end( "Mode::adPlr" );
            return;
        }

    m_lPlr.append( plr );
    plr->setActive( true );

    plrAdded( m_lPlr.back(), this );
    checkPartner( plr, partner );

    m_log.log( "player '" + plr->name() + "' added and set active" );
    m_log.end( "Mode::addPlr" );
}
/*!
 * \brief Adds the result _iBalls_ of a set to match with id _iIDMatch_.
 * \param iIDMatch Unique match id. If no match with such id is found, nothing is done.
 * \param iBalls Match result. If _iBalls_ is not in range of balls allowed by the Match::m_iBalls member, nothing is done.
 * \details Mode::save is called.
 *
 * Emits Mode::addedSet( _iIDMatch_, _iBalls_ ).
 *
 * A call is logged as Mode::addSet
 */
void Mode::addSet(int iIDMatch, int iBalls ){
    m_log.log( "Mode::addedSet( " + QVariant( iIDMatch ).toString() + ", " + QVariant( iBalls ).toString() + " )" );

    foreach( Match* pm, m_lMatch ){
        if( pm->id() == iIDMatch && iBalls >= 0 && iBalls <= pm->balls()){
            pm->addSet( iBalls );            
            addedSet( iIDMatch, iBalls );            
            save();
            return;
        }
    }    
}
/*!
 * \brief Adds table with the name _strTable_ to this Mode object and immediately calls Mode::testForTablesToPlay.
 * \param strTable Name of the new table.
 * \details If _strTable_ is "" or a table with with name _strTable_ already exists, nothing is done
 *
 * Emits Mode::tableAdded( _strTable_ ).
 *
 * A call is logged as Mode::addTable.
 */
void Mode::addTable( QString strTable ){
    m_log.log( "Mode::addTable( " + strTable + " )" );

    if( strTable == "" || m_slTables.contains( strTable )){
        m_log.log( "invalid param '" + strTable + "'" );
        return;}

    m_slTables.append( strTable );

    tableAdded( strTable );
    testForTablesToPlay();
}
/*!
 * \brief If _partner_ is a valid Player object and this Mode object supports fixed teams, _partner_ is set as partner for player _plr_
 * \param plr Player in question.
 * \param partner Pointer to partner in question for this mode. If _NULL_, nothing is done.
 * \details Only sets partner if Mode::m_mode includes following Mode::ModeType flag combinations:
 * - Mode::MODE_PRELIM | Mode::MODE_DOUBLE
 * Otherwise nothing is done.
 *
 * Calls Player::setPartner( this->name(), _partner_ ) and Mode::addPlr( _partner_ ).
 *
 * A call is logged as block Mode::checkPartner.
 */
void Mode::checkPartner( Player* plr, Player* partner ){
    m_log.begin( "Mode::checkPartner" );

    if( !partner ){
        m_log.log( "partner == NULL" );
        m_log.end( "Mode::checkPartner" );
        return;}

    switch( m_mode ){
    case ( MODE_PRELIM | MODE_DOUBLE ):
        break;
    default:{
        m_log.log( "mode doesn't allow fixed teams" );
        m_log.end( "Mode::checkPartner" );
        return;}
    }

    plr->setPartner( name(), partner );
    addPlr( partner );

    m_log.end( "Mode::checkPartner" );
}
/*!
 * \brief Removes table with name _strTable_ from all matches, but the table remains in this Mode object.
 * \param strTable Name of table to remove.
 * \details A call is logged as Mode::freeTable
 */
void Mode::freeTable( QString strTable ){
    m_log.log( "Mode::freeTable( " + strTable + " )" );

    foreach( QString str, m_slTablesInPlay )
        if( str == strTable ){
            foreach( Match* match, m_lMatch )
                if( match->table() == strTable )
                    for( int i = 0; i < 4; i++ )
                        if( match->getPlr( i )) match->getPlr( i )->setIsPlaying( false );
            m_slTables.append( str );
            m_slTablesInPlay.removeOne( str );
            return;
        }
}
/*!
 * \brief Does nothing. Override in derived classes to provide new matches with calls to Mode::addMatch.
 */
void Mode::newRound(){
}
/*!
 * \brief Deletes match with id _iIDMatch_ in this Mode object.
 * \param iIDMatch Id of match to delete.
 * \details If no match with id _iIDMain_ is found, nothing is done.
 *
 * Emits Mode::matchRemoved( _iIDMatch_ )
 *
 * A call is logged as block Mode::removeMatch
 */
void Mode::removeMatch( int iIDMatch ){
    m_log.begin( "Mode::removeMatch" );

    foreach( Match* pm, m_lMatch){
        if( pm->id() == iIDMatch ){
            if( pm->table() != "" )freeTable( pm->table());
            delete pm;
            m_lMatch.removeOne( pm );
            matchRemoved( iIDMatch );

            m_log.log( "removed match " + QVariant( iIDMatch ).toString());
            m_log.end( "Mode::removeMatch" );
            return;
        }
    }

    m_log.log( "no match with id " + QVariant( iIDMatch ).toString() + " found" ) ;
    m_log.end( "Mode::removeMatch" );
}
/*!
 * \brief Removes set at index _iSet_ of the Match::m_liRes member of Match object with id _iIDMatch_.
 * \param iIDMatch Id of match where set will be removed.
 * \param iSet Index of set. If < 0 or >= Match::m_liRes.count() of match _iIDMatch_, nothing is done.
 * \details A call is logged as Mode::removeSet
 */
void Mode::removeSet( int iIDMatch, int iSet ){
    m_log.log( "Mode::removeSet( " + QVariant( iIDMatch ).toString() + ", " + QVariant( iSet ).toString() + " )" );

    Match* pm = getMatch( iIDMatch );

    if( !pm || iSet >= pm->getSetCount()){
        m_log.log( "invalid param" );
        return;}

    pm->removeSet( iSet );
}
/*!
 * \brief Removes table with name _strTable_ from this Mode object.
 * \param strTable Name of table to remove.
 * \details Calls Mode::freeTable( _strTable_ ) and removes it from Mode::m_slTables afterwards.
 * If _strTable_ is "", nothing is done.
 *
 * Emits Mode::tableRemoved( _strTable_ ).
 *
 * A call is logged as Mode::removeTable
 */
void Mode::removeTable( QString strTable ){
    m_log.log( "Mode::removeTable( " + strTable + " )" );

    if( strTable == "" ){
        m_log.log( "invalid param" );
        return;}

    freeTable( strTable );

    if( m_slTables.removeOne( strTable ) || m_slTablesInPlay.removeOne( strTable )){
        foreach ( Match* pm, m_lMatch)
            if( pm->table() == strTable )
                pm->setTable( "" );

        testForTablesToPlay();
    }

    tableRemoved( strTable );
}
/*!
 * \brief Saves the current state of this Mode object to a file named *name_of_mode*.evt.xml.
 * \details During save a backup file of the last save with the extension .tmp is created.
 *
 * A call is logged as Mode::save
 */
//TODO: define file structure for event file version 1.0 and document it
void Mode::save(){
    int iCur;

    if( m_file.isOpen())m_file.close();

    m_file.setFileName( name() + ".evt.xml" );
    m_file.open( QIODevice::WriteOnly | QIODevice::Text );

    if( !m_file.isOpen())return;

    m_xml.setDevice( &m_file );
    m_xml.setAutoFormatting( true );
    m_xml.writeStartDocument();
    m_xml.writeStartElement( "event" );
    m_xml.writeTextElement( "mode" , QVariant( m_mode ).toString());
    m_xml.writeTextElement( "name", m_strName );
    m_xml.writeTextElement( "balls", QVariant( m_iBalls ).toString());
    m_xml.writeTextElement( "sets", QVariant( m_iSets ).toString());
    m_xml.writeStartElement( "player" );

    for( iCur = 0; iCur < m_lPlr.count(); iCur++)
        m_xml.writeAttribute( m_lPlr.at( iCur )->name(), m_lPlr.at( iCur )->partner( name()) ? m_lPlr.at( iCur )->partner( name())->name() : "" );

    m_xml.writeEndElement();
    m_xml.writeTextElement( "matchCount", QVariant( m_lMatch.count()).toString());
    m_xml.writeStartElement( "matches" );

    for( iCur = 0; iCur < m_lMatch.count(); iCur ++ ){
        m_xml.writeStartElement( "match" + QVariant( iCur ).toString());
        m_xml.writeAttribute( "p11" , m_lMatch.at( iCur )->getPlr( 0 )->name());
        m_xml.writeAttribute( "p12", ( m_mode & MODE_DOUBLE ) ? m_lMatch.at( iCur )->getPlr( 1 )->name() : "" );
        m_xml.writeAttribute( "p21", m_lMatch.at( iCur )->getPlr( 2 )->name());
        m_xml.writeAttribute( "p22", ( m_mode & MODE_DOUBLE ) ? m_lMatch.at( iCur )->getPlr( 3 )->name() : "" );
        m_xml.writeAttribute( "result", QVariant( m_lMatch.at( iCur )->getSet( 0 )).toString());
        m_xml.writeEndElement();
    }

    m_xml.writeEndElement(); //matches
    m_xml.writeEndElement(); //event
    m_xml.writeEndElement();
    m_xml.writeEndDocument();

    while( !m_file.flush());
    m_file.close();
    m_file.setFileName( name() + ".evt.xml.tmp" );
    m_file.remove();
    m_file.setFileName( name() + "evt.xml" );
    m_file.rename( name() + ".evt.xml.tmp" );
}
/*!
 * \brief Sets the Mode::m_mode member of this Mode object. See Mode::ModeType for supported values
 * \param mode Combination of Mode::ModeType flags describing the event mode.
 * \details If _mode_ equals the m_mode member of this Mode object, nothing is done.
 *
 * Emits Mode::onModeChanged.
 *
 * A call is logged as Mode::setMode( _mode_ ).
 */
void Mode::setMode(unsigned int mode ){
    m_log.log( "Mode::setMode( " + QVariant( mode ).toString() + " )" );

    if( mode == m_mode )return;

    m_mode = mode;

    onModeChanged();
}
/*!
 * \brief Sets the number of balls currently played per set to _iBalls_ for this Mode object.
 * \param iBalls Maximum number of balls played per set. Even values lead to possibly drawn sets.
 * \details If _iBalls_ is < 0 or _iBalls_ is > 19 or _iBalls_ equals the m_iBalls member of this Mode object, nothing is done.
 *
 * Emits Mode::onBallsChanged.
 *
 * A call is logged as Mode::setBalls( _iBalls_ ).
 */
void Mode::setBalls( int iBalls ){
    m_log.log( "Mode::setBalls( " + QVariant( iBalls ).toString() + " )" );

    if( iBalls <= 0 || iBalls > 19 || iBalls == m_iBalls )return;

    m_iBalls = iBalls;

    onBallsChanged();
}
/*!
 * \brief Sets name of this Mode object.
 * \param strName New name of this Mode object.
 * \details If _strName_ is "" or strName equals the m_strName member of this Mode object, nothing is done.
 *
 * Emits Mode::onNameChanged.
 *
 * A call is logged as Mode::setName( _strName_ ).
 */
void Mode::setName( QString strName ){
    m_log.log( "Mode::setName( " + strName + " )" );

    if( strName == "" || strName == m_strName )return;

    m_strName = strName;

    onNameChanged();
}
/*!
 * \brief Sets the current number of sets played per match to _iSets_ in this Mode object.
 * \param iSets Maximum number of sets played per match. Even values lead to possibly drawn matches.
 * \details If _iSets_ is <= 0 or _iSets_ is > 9 or iSets equals the m_iSets member of this Mode object, nothing is done.
 *
 * Emits Mode::onSetsChanged.
 *
 * A call is logged as Mode::setSets( _iSets_ )
 */
void Mode::setSets( int iSets ){
    m_log.log( "Mode::setSets( " + QVariant( iSets ).toString() + " )" );

    if( iSets <= 0 || iSets > 9 || iSets == m_iSets )return;

    m_iSets = iSets;

    onSetsChanged();
}
/*!
 * \brief Tests for each match with no table set in this Mode object if the players in these matches are currently in a playing state.
 * If they are not playing and a free table is available to this Mode object, the table is set to that match and the players playing state is set
 * to true.
 * \details Emits Mode::tableSetToPlay( **match_id**, **table_name** ).
 *
 * A call is logged as Mode::testForTablesToPlay.
 */
void Mode::testForTablesToPlay(){
    bool bNobodyIsPlaying;
    int iCur;

    m_log.log( "Mode::testForTablesToPlay" );

    foreach( Match* pm, m_lMatch){
        if( !m_slTables.count())return;
        if( pm->table() == "" && !pm->getSetCount()){
            bNobodyIsPlaying = true;

            for( iCur = 0; iCur < 4; iCur++ )
                if( pm->getPlr( iCur ) && pm->getPlr( iCur )->isPlaying())
                    bNobodyIsPlaying = false;

            if( bNobodyIsPlaying ){
                pm->setTable( m_slTables.first());
                m_slTablesInPlay.append( m_slTables.first());
                m_slTables.removeOne( m_slTables.first());

                for( iCur = 0; iCur < 4; iCur++ )
                    if( pm->getPlr( iCur ))pm->getPlr( iCur )->setIsPlaying( true );

                tableSetToPlay( pm->id(), pm->table());

                m_log.log( "set table '" + pm->table() + "' to match " + QVariant( pm->id()).toString());
            }
        }
    }
}
