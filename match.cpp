#include "match.h"

int Match::m_siID = 0;

/*!
 * \brief Standard constructor.
 * \param parent Set this to a valid QObject pointer to make this Match object known to the Qt framework.
 */
Match::Match(QObject *parent) : QObject(parent)
{
    for( int i = 0; i < 4; i++ )m_plr[ i ] = NULL;

    m_siID++;
}
/*!
 * \brief Standard deconstructor.
 */
Match::~Match()
{

}
/*!
 * \brief Returns true if this Match object is identical to _pm_, otherwise false.
 * \param pm Pointer to the Match object to be tested.
 * \return _True_ if both Match objects have the same id, otherwise _false_.
 */
bool Match::operator==( Match* pm ){
    if( m_ciID == pm->id())return true;
    else return false;
}
/*!
 * \brief Returns the set at index _iIndex_ in Match::m_liRes.
 * \param iIndex Index of set to return.
 * \return Set result at Match::m_liRes[ _iIndex_ ] if _iIndex is >= 0 and _iIndex is < Match::m_liRes.count(), otherwise _-1_.
 */
int Match::getSet( int iIndex ){
    if( iIndex < 0 || iIndex >= m_liRes.count())return -1;

    return m_liRes.at( iIndex );
}
/*!
 * \brief Appends set with result  _iGoalsTeamOne_ to this Match object.
 * \param iGoalsTeamOne Result of the new set.
 * \details Emits Match::addSet( _iGoalsTeamOne_ ).
 */
void Match::addSet( int iGoalsTeamOne ){
    m_liRes.append( iGoalsTeamOne );

    addedSet( iGoalsTeamOne );
}
/*!
 * \brief Removes set at index _index_ from Match::m_liRes.
 * \param index Index of set to remove.
 * \details If _index_ is < 0 or _index_ is >= Match::m_liRes.count(), no match result is removed.
 *
 * Emits Match::removedSet( _index_ ).
 */
void Match::removeSet( int index ){
    if( index < 0 || index >= m_liRes.count())return;

    m_liRes.removeAt( index );

    removedSet( index );
}
/*!
 * \brief Sets number of balls played per set.
 * \param iBalls Maximum number of balls played in a single set. Even values lead to a possible drawn set.
 * \details Emits Match::onBallsChanged() if _iBalls_ >= 0 and _iBalls_ != Match::m_iBalls.
 */
void Match::setBalls( int iBalls ){
    if( iBalls < 0 || iBalls == m_iBalls )return;

    m_iBalls = iBalls;

    onBallsChanged();
}
/*!
 * \brief Sets position _iPos_ to player _plr_.
 * \param iPos Position to set. Valid values are 0..3.
 * \param plr Pointer to Player object to set. May be _NULL_.
 * \details Emits Match::plrChanged( _iPos_, _plr_ ), if player is not already set and _iPos_ is valid. Otherwise nothing is done.
 */
void Match::setPlr( int iPos, Player* plr ){
    if( iPos < 0 || iPos > 3 || m_plr[ iPos ] == plr )return;

    m_plr[ iPos ] = plr;

    plrChanged( iPos, plr );
}
/*!
 * \brief Sets number of sets played per match.
 * \param iSets Maximum number of sets played per match. Even values lead to a possible drawn match. Valid values are 1..9.
 * \details Emits Match::onSetsChanged(), if _iSets_ is valid and _iSets_ != Match::m_iSets. Otherwise nothing is done.
 */
void Match::setSets( int iSets ){
    if(  iSets <= 0 || iSets > 9 || iSets == m_iSets )return;

    m_iSets = iSets;

    onSetsChanged();
}
/*!
 * \brief Sets name of the table where the match is played.
 * \param strTable Name of table to set. May be _NULL_ or "".
 * \details Emits Match::onTableChanged() if _strTable_ != Match::m_strTable.
 */
void Match::setTable( QString strTable ){
    if( strTable == m_strTable )return;

    m_strTable = strTable;

    onTableChanged();
}
