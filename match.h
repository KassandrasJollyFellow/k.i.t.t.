#ifndef MATCH_H
#define MATCH_H

#include <QObject>
#include <QList>

#include "player.h"

/*!The Match class manages a single foosball match of 2 or 4 players.
 *
 * This class is registered in the QML framework as _KMatch_ version 1.0.
 */
class Match : public QObject
{
    Q_OBJECT
    /*! QML property balls*/
    Q_PROPERTY( int balls READ balls WRITE setBalls NOTIFY onBallsChanged)
    /*!QML property id*/
    Q_PROPERTY( int id READ id )
    /*!QML property sets*/
    Q_PROPERTY( int  sets READ sets WRITE setSets NOTIFY onSetsChanged)
    /*!QML property table*/
    Q_PROPERTY( QString table READ table WRITE setTable NOTIFY onTableChanged)
public:
    explicit Match(QObject *parent = 0);
    ~Match();

    bool operator==( Match* pm );

signals:
    void addedSet( int iGoalsTeamOne );/*!<Emitted when a new set with the result _iGoalsTeamOne_ was added to this Match object.*/
    void onBallsChanged();/*!<Emitted when Match::m_iBalls has changed.*/
    void onTableChanged();/*!<Emitted when Match::m_strTable has changed.*/
    void onSetsChanged();/*!<Emitted when Match::m_iSets has changed.*/
    void plrChanged( int iPos, Player* plr );/*!<Emitted when the player at position _iPos_ has changed to _plr_.*/
    void removedSet( int index );/*!<Emitted when the set at index _index_ was removed from Match::m_liRes.*/

public slots:
    inline int balls(){ return m_iBalls;}/*!<Returns Match::m_iBalls.*/
    inline int id(){ return m_ciID;}/*!<Returns Match::m_ciID.*/
    inline int getSetCount(){ return m_liRes.count();}/*!<Returns Match::m_liRes.count().*/
    inline int sets(){ return m_iSets;}/*!<Returns Match::m_iSets.*/
    /*!Returns _NULL_, if _iIndex_ < 0 or _iIndex_ >= 4. Otherwise Match::m_plr[ _iIndex_ ].*/
    inline Player* getPlr( int iIndex ){ if( iIndex >= 0 && iIndex < 4 )return m_plr[ iIndex ];else return NULL;}
    inline QString table(){ return m_strTable;}/*!<Returns Match::m_strTable.*/

    int getSet( int iIndex );

    void addSet( int iGoalsTeamOne );
    void removeSet(int index );
    void setBalls( int iBalls );
    void setPlr( int iPos, Player* plr );
    void setSets(int iSets);
    void setTable( QString strTable );
protected:
    const int m_ciID = m_siID; /*!<Holds the unique match id.*/
private:
    int m_iBalls;/*!<Number of balls played out per set.*/
    int m_iSets;/*!<Number of sets played out.*/
    /*!Players in this match. Ordering is { _PlayerOneTeamOne_, _PlayerTwoTeamOne_, _PlayerOneTeamTwo_,
     * _PlayerTwoTeamTwo_ }. If there are only two players, the second and fourth element are set to _NULL_.
    */
    Player* m_plr[ 4 ];
    QList< int > m_liRes;/*!<Holds the results of the sets played.*/
    QString m_strTable;/*!<Name of the table for this match. "" if no table is set.*/
    static int m_siID;/*!<Tracks number of Match objects to maintain unique ids.*/
};

#endif // MATCH_H
