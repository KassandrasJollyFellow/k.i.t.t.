import QtQuick 2.4

import KMode 1.0
import KRanking 1.0
import KRanks 1.0
import Logger 1.0

RankingForm {
    property KRanks ranks

    Logger{
        id: logRanks
    }

    Connections{
        target: kitt
        onModeAdded: modelModes.append({ mode: mode.name })
        onModeDeleted:{
            for( var i = 0; i < modelModes.count; i++ )
                if( strName === modelModes.get( i ).mode ){
                    modelModes.remove( i )
                    return
                }
        }
    }

    Connections{
        target: tableModes
        onClicked: getRanks()
    }    

    onVisibleChanged: if( visible ) getRanks()

    Component.onCompleted:  logRanks.log( "Ranking completed" )

    function getRanks(){
        ranks = kitt.getMode( modelModes.get( tableModes.currentRow ).mode ).getRanks()
        modelRankings.clear()

        for( var i = 0; i < ranks.count(); i++ )
            modelRankings.append({ rank: ranks.get( i ).rank, player: ranks.get( i ).player,
                                 wins: ranks.get( i ).wins, draws: ranks.get( i ).draws,
                                 loose: ranks.get( i ).loose, points: ranks.get( i ).points,
                                 buchholz: ranks.get( i ).buchholz, sb: ranks.get( i ).sb })
    }
}

