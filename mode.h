#ifndef MODE_H
#define MODE_H

#include <QFile>
#include <QObject>
#include <QList>
#include <QXmlStreamReader>
#include <QXmlStreamWriter>
#include <QStringList>

#include "logger.h"
#include "ranks.h"

/*!
 * \brief The Mode class is the base class for the different possible event modes. It supports  their common behaviour and
 * communication with the QML framework.
 * \details This class is registered in the QML framework as _KMode_ version 1.0.
 */
class Mode : public QObject
{
    Q_OBJECT
    Q_ENUMS( ModeType )
    /*! \brief QML property balls*/
    Q_PROPERTY( int balls READ balls WRITE setBalls NOTIFY onBallsChanged)
    /*! \brief QML property round*/
    Q_PROPERTY( int round READ round NOTIFY onRoundChanged)
    /*! \brief QML property sets*/
    Q_PROPERTY( int sets READ sets WRITE setSets NOTIFY onSetsChanged)
    /*! \brief QML property mode*/
    Q_PROPERTY(unsigned int mode READ mode WRITE setMode NOTIFY onModeChanged)
    /*! \brief QML property name*/
    Q_PROPERTY(QString name READ name WRITE setName NOTIFY onNameChanged)
public:
    explicit Mode(QObject *parent = 0);
    ~Mode();

    bool operator==( Mode* pm );
    /*!
     * \brief The ModeType enum holds flags, that can be combined to describe the event mode.
     * \details Currently supported combinations are:
     * - Mode::MODE_PRELIM single preliminary rounds
     * - Mode::MODE_PRELIM | Mode::MODE_DOUBLE double preliminary rounds
     * - Mode::MODE_PRELIM | Mode::MODE_DOUBLE | Mode::MODE_DYP D.Y.P. preliminary rounds
     * - Mode::MODE_MDYP Monster D.Y.P.
     */
    enum ModeType : unsigned int{
        MODE_SINGLE = 0, /*!< 0 - placeholder value*/
        MODE_DOUBLE = 1,/*!< 1 - indicates two player per team*/
        MODE_DYP = 2,/*!< 2 - indicates D.Y.P. mode*/
        MODE_PRELIM = 4,/*!< 4 - indicates preliminary rounds*/
        MODE_MDYP = 15,/*!< 15 - Monster D.Y.P. mode. Implies MODE_DOUBLE | MODE_DYP | MODE_PRELIM*/
        MODE_UNKNOWN = 16,/*!< 16 - placeholder value used for dummy Mode objects.*/
        MODE_FEELGOOD = 32/*!< 32 - Used in dyp modes. If set, pairings of each round are rerolled if neccessary to achieve even distribution*/
    };

signals:
    void addedSet( int idMatch, int iBalls ); /*!< \brief Emitted when set result _iBalls_ has been added to the Match object with id _idMatch_.*/
    /*!
     * \brief Emitted when players _strPlr_ and _strPartner_ were added to Mode object _mode_. _strPartner_ can be _NULL_.
     * \param mode Pointer to this Mode object. Used to identify the mode the signal originates from.
     * \param strPlr Name of first player in team.
     * \param strPartner Name of second player in team if there is one, otherwise _NULL_ or "".
     */
    void loadedPlr( Mode* mode, QString strPlr, QString strPartner );
    void matchAdded( Match* match, Mode* mode );/*!< \brief Emitted when Match object _match_ was added to Mode object _mode_.*/
    void matchRemoved( int id );/*!< \brief Emitted when Match object with id _id_ was removed from this Mode object.*/
    void onBallsChanged();/*!< \brief Emitted when Mode::m_iBalls has changed.*/
    void onModeChanged();/*!< \brief Emitted when Mode::m_mode has changed.*/
    void onNameChanged();/*!< \brief Emitted when Mode::m_strName has changed.*/
    void onRoundChanged();/*!< \brief Emitted when Mode::m_iRound has changed.*/
    void onSetsChanged();/*!< \brief Emitted when Mode::m_iSets has changed.*/
    void plrAdded( Player* plr, Mode* mode );/*!< \brief Emitted when Player object _plr_ was added to Mode object _mode_.*/
    void plrRemoved( Player* plr, Mode* mode );/*!< \brief Emitted when player _plr_ was removed from Mode object _mode_.*/
    void tableAdded( QString table );/*!< \brief Emitted when table with name _table_ was added to this Mode object.*/
    void tableRemoved( QString table );/*!< \brief Emitted when table with name _table_ was removed from this Mode object.*/
    void tableSetToPlay( int id, QString table );/*!< \brief Emitted when table with name _table_ was set in Match object with id _id_.*/
public slots:
    bool loadEvent( QString strFile );
    /*!
     * \brief Tests if _strTable_ is the name of a table in this Mode object.
     * \param strTable Table name to test.
     * \return _True_ if a table with name _strTable_ exists in this Mode object, otherwise _false_.
     */
    inline bool isTable( QString strTable ){ if( m_slTables.contains( strTable ))return true; if( m_slTablesInPlay.contains( strTable ))return true; return false;}    
    inline int balls(){ return m_iBalls;}/*!<Returns Mode::m_iBalls.*/
    inline int getMatchCount(){ return m_lMatch.count();}/*!<Returns Mode::m_lMatch.count().*/
    inline int getPlrCount(){ return m_lPlr.count();}/*!<Returns Mode::m_lPlr.count().*/
    inline int round(){ return m_iRound;}/*!<Returns Mode::m_iRound.*/
    inline int sets(){ return m_iSets;}/*!<Returns Mode::m_iSets.*/
    /*! \brief If this Mode object contains a Match object with id _id_ a pointer to it is returned, otherwise _NULL_.*/
    inline Match* getMatch( int id ){ foreach( Match* pm, m_lMatch )if( id == pm->id())return pm; return NULL;}    
    inline Match* getMatchAtIndex( int index ){ if( index < 0 || index >= m_lMatch.count()) return NULL; return m_lMatch.at( index );}
    inline Player* getPlr( int index ){ if( index < 0 || index >= m_lPlr.count()) return NULL; return m_lPlr.at( index );}
    /*! \brief If this Mode object contains a Player object with the name _strPlr_ a pointer to it is returned, otherwise _NULL_.*/
    inline Player* getPlr( QString strPlr ){ foreach( Player* plr, m_lPlr )if( plr->name() == strPlr )return plr; return NULL;}
    inline QString name(){ return m_strName;}/*!<Returns Mode::m_strName.*/
    inline unsigned int mode(){ return m_mode;}/*!<Returns Mode::m_mode.*/
    /*!
     * \brief Calls Mode::matchAdded for each existing Match object in this Mode object as if they had just been created.
     * Used to parse matches loaded from a file to the GUI.
     */
    inline void signalMatches(){ foreach( Match* match, m_lMatch )matchAdded( match, this );}

    Player* getSupportPlr( int idMatch );

    virtual Ranks* getRanks();/*!< \brief Returns _NULL_. Dervied classes need to override this function to provide a ranking.*/
    virtual void newRound();/*!< \brief Does nothing. Derived classes need to override this function to provide new matches for a requested round.*/

    void addPlr(Player* plr , Player *partner = NULL );
    void addTable( QString strTable );
    void addSet( int iIDMatch, int iBalls );
    void freeTable( QString strTable );
    void removeMatch( int iIDMatch );
    void removeTable( QString strTable );
    void removeSet( int iIDMatch, int iSet );
    void setBalls( int iBalls );
    void setMode( unsigned int mode );
    void setName( QString strName );    
    void setSets( int iSets );
    void testForTablesToPlay();
protected:
    bool addMatch( Match* pm );

    int m_iBalls; /*!< \brief Holds the current number of balls per set in this Mode object.*/
    int m_iRound;/*!< \brief Holds the current round index of this Mode object.*/
    Logger m_log;/*!< \brief Logger object for the Mode class and its derived classes.*/
    unsigned int m_mode;/*!< \brief Holds a combination of Mode::ModeType flags.*/
    QList< Match* > m_lMatch;/*!< \brief Holds all Match objects belonging to this Mode object.*/
    QList< Player* > m_lPlr;/*!< \brief Holds all Player objects belonging to this Mode object.*/
    Ranks m_ranks;/*!< \brief Holds the rankings generated last.*/
private:    
    /*!
     * \brief Returns a QStringRef to the value read from xml stream _px_.
     * \param px Pointer to a xml stream reader.
     * \return Returns the text value of the item read from _px_.
     * \warning Does not test for valid return value.
     */
    inline QStringRef getXmlVal( QXmlStreamReader* px ){ px->readNext(); return px->text();}
    void checkPartner( Player* plr, Player* partner );
    void save();

    int m_iSets;/*!< \brief Current number of sets per match in this event.*/
    QFile m_file;/*!< \brief File containing backup of this event. Do not rely on this value as it might changed often.*/
    QString m_strName;/*!< \brief Name of this event.*/
    QStringList m_slTables;/*!< \brief Names of tables belonging to this Mode object.*/
    QStringList m_slTablesInPlay;/*!< \brief Names of tables currently occupied. Tables stored in Mode::m_slTablesInPlay dont appear in Mode::m_slTables.*/
    QXmlStreamWriter m_xml;/*!< \brief Xml stream used to store backup of this event.*/
};

Player* leastByes( QList< Player* >* plplr );

#endif // MODE_H
