import QtQuick 2.4
import QtQuick.Layouts 1.0
import QtQuick.Controls 1.2

Item {
    id: dbg
    anchors.fill: parent

    Rectangle {
        id: rectangle1
        color: "#000000"
        border.width: 0
        anchors.fill: parent
    }

    GridLayout{
        anchors.fill: parent
        anchors.margins: 10
        columns: 3
        Label{
            color: "white"
            text: "Spieler:"
        }
        Label{
            id: labelPlr
            color: "white"
            text: sliderPlr.value
        }
        Slider{
            id: sliderPlr
            Layout.fillWidth: true
            maximumValue: 1000
            minimumValue: 10
            stepSize: 1
        }
        Label{
            color: "white"
            text: "Tische:"
        }
        Label{
            id: labelTables
            color: "white"
            text: sliderTable.value
        }
        Slider{
            id: sliderTable
            Layout.fillWidth: true
            maximumValue: 100
            minimumValue: 1
            stepSize: 1
        }
        Label{
            color: "white"
            text: "Bälle: "
        }
        Label{
            color: "white"
            text: sliderBall.value
        }
        Slider{
            id: sliderBall
            Layout.fillWidth: true
            maximumValue: 19
            minimumValue: 9
            stepSize: 1
        }
        Label{
            color: "white"
            text: "Sätze: "
        }
        Label{
            color: "white"
            text: sliderSet.value
        }
        Slider{
            id: sliderSet
            Layout.fillWidth: true
            maximumValue: 9
            minimumValue: 1
            stepSize: 1
        }
        Label{
            color: "white"
            text: "Runden: "
        }
        Label{
            id: labelRounds
            color: "white"
            text: sliderRound.value
        }
        Slider{
            id: sliderRound
            Layout.fillWidth: true
            maximumValue: 1000
            minimumValue: 10
            stepSize: 1
        }
        Button{
            id: btnStart
            text: "Start"
        }
        ProgressBar{
            id: progress
            Layout.fillWidth: true
            maximumValue: 100
            minimumValue: 1
        }
        Button{
            id: btnDump
            text: "Dump"
        }

        TextArea{
            id: txtDbg
            backgroundVisible: false
            Layout.fillHeight: true
            Layout.fillWidth: true
            textColor: "white"
        }
    }

    property alias btnDump: btnDump
    property alias btnStart: btnStart
    property alias progress: progress
    property alias sliderBall: sliderBall
    property alias sliderPlr: sliderPlr
    property alias sliderRound: sliderRound
    property alias sliderSet: sliderSet
    property alias sliderTable: sliderTable
    property alias txtDbg: txtDbg
}

