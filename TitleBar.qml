import QtQuick 2.4
import QtQuick.Window 2.0

import "qrc:/Debug"

TitleBarForm {
    property var pos

    Timer{
        id: clock
        interval: 1000
        repeat: true
        triggeredOnStart: true
        onTriggered:{
            labelClock.text = Qt.formatTime( new Date)
        }
    }

    Connections{
        target: maCls
        onHoveredChanged:{
            if( maCls.containsMouse ) checkCls4Sure.visible = true
            else{
                checkCls4Sure.visible = false
                checkCls4Sure.checked = false
            }
        }
    }
    Connections{
        target: maCls4Sure
        onClicked: if( checkCls4Sure.checked )close()
    }

    Connections{
        target: maMin
        onClicked: aw.showMinimized()
        onHoveredChanged:{
            if( maMin.containsMouse ) labelMin.color = "red"
            else labelMin.color = "white"
        }
    }

    Connections{
        target: maMain
        onDoubleClicked:{
            if( mouse.buttons & Qt.RightButton ){
                tabMain.addTab( "Debug" )
                Qt.createQmlObject( 'import "qrc:/Debug"; Debug{}', tabMain.getTab( tabMain.count - 1 ))
                return
            }else if( mouse.buttons & Qt.LeftButton )
                aw.showFullScreen()
        }

        onEntered: rDrag.color = "red"
        onExited: rDrag.color = "black"
        onPositionChanged:{
            if( !maMain.pressed || !( maMain.pressedButtons & Qt.LeftButton ))return
            var delta = Qt.point( mouse.x - pos.x, mouse.y - pos.y )
            aw.x += delta.x
            aw.y += delta.y
        }
        onPressed: pos = Qt.point( mouse.x, mouse.y )
    }

    Connections{
        target: maMax
        onClicked: aw.showFullScreen()
        onHoveredChanged:{
            if( maMax.containsMouse )labelMax.color = "red"
            else labelMax.color = "white"
        }
    }

    onWidthChanged: {
        visible = false
        visible = true
    }

    Component.onCompleted: clock.start()
}

