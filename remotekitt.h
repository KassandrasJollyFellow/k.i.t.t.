#ifndef REMOTEKITT_H
#define REMOTEKITT_H

#include <QObject>

#include "kitt.h"

class RemoteKITT : public QObject
{
    Q_OBJECT
    Q_ENUMS( Msg )
    Q_PROPERTY( int dbgBallCount READ dbgBallCount WRITE setDbgBallCount NOTIFY onDbgBallCountChanged)
    Q_PROPERTY( int dbgPlrCount READ dbgPlrCount WRITE setDbgPlrCount NOTIFY onDbgPlrCountChanged)
    Q_PROPERTY( int dbgRoundCount READ dbgRoundCount WRITE setDbgRoundCount NOTIFY onDbgRoundCountChanged)
    Q_PROPERTY( int dbgSetCount READ dbgSetCount WRITE setDbgSetCount NOTIFY onDbgSetCountChanged)
    Q_PROPERTY( int dbgTableCount READ dbgTableCount WRITE setDbgTableCount NOTIFY onDbgTableCountChanged)
public:
    explicit RemoteKITT(QObject *parent = 0);
    ~RemoteKITT();

    enum Msg : unsigned int{
        RKM_CREATEPLR,
        RKM_CREATETABLE,
        RKM_DBG,
        RKM_DBG_SETBALL,
        RKM_DBG_SETPLR,
        RKM_DBG_SETROUND,
        RKM_DBG_SETSET,
        RKM_DBG_SETTABLE,
        RKM_FINISHED,
        RKM_INIT,
        RKM_LOG,
        RKM_OK,
        RKM_PROGRESS,
        RKM_UNKNOWN
    };
signals:        
    void onDbgBallCountChanged();
    void onDbgPlrCountChanged();
    void onDbgRoundCountChanged();
    void onDbgSetCountChanged();
    void onDbgTableCountChanged();
    void msgOut( const int &msg, const QVariant &var );
    void progress( QVariant varProcess );
public slots:        
    inline int dbgBallCount(){ return m_iDbgBallCount;}
    inline int dbgPlrCount(){ return m_iDbgPlrCount;}
    inline int dbgRoundCount(){ return m_iDbgRoundCount;}
    inline int dbgSetCount(){ return m_iDbgSetCount;}
    inline int dbgTableCount(){ return m_iDbgTableCount;
                              }
    void msgIn( const int &msg, const QVariant &var );
    void setDbgBallCount( int iCount );
    void setDbgPlrCount( int iCount );
    void setDbgRoundCount( int iCount );
    void setDbgSetCount( int iCount );
    void setDbgTableCount( int iCount );
private:
    void debug(const QVariant &var);
    void init();

    int m_iDbgBallCount;
    int m_iDbgPlrCount;
    int m_iDbgRoundCount;
    int m_iDbgSetCount;
    int m_iDbgTableCount;

    KITT* m_pk;
};

#endif // REMOTEKITT_H
