#ifndef PLAYER_H
#define PLAYER_H

#include <QList>
#include <QObject>
#include <QtQml/QQmlListProperty>

class Player;

class TeamUp : public QObject{
    Q_OBJECT
    Q_PROPERTY( QString  mode READ mode WRITE setMode NOTIFY onModeChanged)
    Q_PROPERTY( Player* player READ player WRITE setPlayer NOTIFY onPlayerChanged)
public:
    explicit TeamUp( QObject* parent = 0 );
    ~TeamUp();

signals:
    void onModeChanged();
    void onPlayerChanged();

public slots:
    inline Player* player(){ return m_plr;}
    inline QString mode(){ return m_strMode;}

    void setMode( QString strMode );
    void setPlayer( Player* plr );
private:
    Player* m_plr;
    QString m_strMode;
};

class Player : public QObject
{
    Q_OBJECT
    Q_PROPERTY( bool active READ active WRITE setActive NOTIFY onActiveChanged)
    Q_PROPERTY( bool isPlaying READ isPlaying WRITE setIsPlaying NOTIFY onIsPlayingChanged)
    Q_PROPERTY( int byes READ byes WRITE setByes NOTIFY onByesChanged)
    Q_PROPERTY( QQmlListProperty< TeamUp > teamUps READ teamUps )
    Q_PROPERTY( QString name READ name WRITE setName NOTIFY onNameChanged)
public:
    explicit Player(QObject *parent = 0);
    ~Player();

    bool operator==( Player* plr );

    inline int teamUpCount(){ return m_teamUps.count();}

    QQmlListProperty< TeamUp > teamUps();

    TeamUp* teamUp( int index ) const;

signals:
    void onActiveChanged();
    void onByesChanged();
    void onIsPlayingChanged();
    void onNameChanged();
public slots:
    inline bool active(){ return m_bActive;}
    inline bool isPlaying(){ return m_bIsPlaying;}
    inline const QString name(){ return m_strName;}
    inline int byes(){ return m_iByes;}   
    inline void setPartner( QString strMode, Player* partner ){ setPartner( strMode, partner, false );}

    Player* partner( QString strMode );

    void setActive( bool bActive );
    void setByes( int iByes );
    void setName( QString strName );
    void setIsPlaying( bool bIsPlaying );
    void setPartner( QString strMode, Player* partner, bool bPartnerSet );
private:    

    bool m_bActive;
    bool m_bIsPlaying;
    int m_iByes;
    QList< TeamUp* > m_teamUps;
    QString m_strName;    
};

#endif // PLAYER_H
