import QtQuick 2.0

Text{
    text: styleData.column === 0 ? modelRankings.get( styleData.row ).rank : styleData.column === 1 ?
          modelRankings.get( styleData.row ).player.name : styleData.column === 2 ?
          (( modelRankings.get( styleData.row ).wins + modelRankings.get( styleData.row ).draws +
          modelRankings.get( styleData.row ).loose ) + " / " + modelRankings.get( styleData.row ).wins +
          " / " + modelRankings.get( styleData.row ).draws + " / " + modelRankings.get( styleData.row ).loose ) :
          styleData.column === 3 ? modelRankings.get( styleData.row ).points : styleData.column === 4 ?
          modelRankings.get( styleData.row ).buchholz : modelRankings.get( styleData.row ).sb
    color: modelRankings.get( styleData.row ).player.active ? "black" : "grey"
    font.bold: true
    fontSizeMode: Text.VerticalFit
}

