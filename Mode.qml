import QtQuick 2.4

import KMode 1.0
import Logger 1.0

import "qrc:/Mode"

ModeForm {
    KMode{
        id: km
    }

    Logger{
        id: logMode
    }

    Connections{
        target: editTable
        onAccepted:{
            kitt.newTable( editTable.text )
            editTable.text = ""
        }
    }
    Connections{
        target: btnAddTable
        onClicked: editTable.accepted()
    }
    Connections{
        target: btnKillTable
        onClicked: kitt.deleteTable( editTable.text )
    }

    Connections{
        target: kitt
        onTableAdded: modelTables.append({ table: strTable })
        onTableKilled:{
            for( var i = 0; i < modelTables.count; i++ )
                if( modelTables.get( i ).table === strTable ){
                    modelTables.remove( i )
                    break
                }
        }
    }

    Connections{
        target: tableTables
        onClicked: editTable.text = modelTables.get( tableTables.currentRow ).table        
    }

    Component.onCompleted: kitt.loadTableBase( "kitt.tbl" )
}

