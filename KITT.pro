TEMPLATE = app

QT += qml quick widgets

CONFIG += c++14

#uncomment to force use of opengl32.dll on windows
#LIBS += -lopengl32

SOURCES += main.cpp \
    kitt.cpp \
    player.cpp \
    mode.cpp \
    match.cpp \
    ranking.cpp \
    ranks.cpp \
    logger.cpp \
    prelim.cpp \
    remotekitt.cpp \
    debugger.cpp

RESOURCES += qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
include(deployment.pri)

HEADERS += \
    kitt.h \
    player.h \
    mode.h \
    match.h \
    ranking.h \
    ranks.h \
    logger.h \
    prelim.h \
    remotekitt.h \
    debugger.h

DISTFILES +=
