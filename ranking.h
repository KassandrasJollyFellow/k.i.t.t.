#ifndef RANKING_H
#define RANKING_H

#include <QObject>

#include "player.h"

class Ranking : public QObject
{
    Q_OBJECT
    Q_PROPERTY( int buchholz READ buchholz WRITE setBuchholz NOTIFY onBuchholzChanged)
    Q_PROPERTY( int draws READ draws WRITE setDraws NOTIFY onDrawsChanged)
    Q_PROPERTY( int loose READ loose WRITE setLoose NOTIFY onLooseChanged)
    Q_PROPERTY( Player* player READ player WRITE setPlayer NOTIFY onPlayerChanged)
    Q_PROPERTY( int points READ points WRITE setPoints NOTIFY onPointsChanged)
    Q_PROPERTY( int rank READ rank WRITE setRank NOTIFY onRankChanged)
    Q_PROPERTY( int sb READ sb WRITE setSb NOTIFY onSbChanged)
    Q_PROPERTY( int wins READ wins WRITE setWins NOTIFY onWinsChanged)
public:
    explicit Ranking(QObject *parent = 0);
    ~Ranking();

signals:
    void onBuchholzChanged();
    void onDrawsChanged();
    void onLooseChanged();
    void onPlayerChanged();
    void onPointsChanged();
    void onRankChanged();
    void onSbChanged();
    void onWinsChanged();
public slots:
    inline int buchholz(){ return m_iBuchholz;}
    inline int draws(){ return m_iDraws;}
    inline int loose(){ return m_iLoose;}
    inline int points(){ return m_iPoints;}
    inline int rank(){ return m_iRank;}
    inline int sb(){ return m_iSb;}
    inline int wins(){ return m_iWins;}
    inline Player* player(){ return m_plr;}
    inline void setBuchholz( int iBuchholz ){ if( iBuchholz == m_iBuchholz )return; m_iBuchholz = iBuchholz; onBuchholzChanged();}
    inline void setDraws( int iDraws ){ if( iDraws == m_iDraws )return; m_iDraws = iDraws; onDrawsChanged();}
    inline void setLoose( int iLoose ){ if( iLoose == m_iLoose )return; m_iLoose = iLoose; onLooseChanged();}
    inline void setPlayer( Player* plr ){ if( plr == 0 || plr == m_plr )return; m_plr = plr; onPlayerChanged();}
    inline void setPoints( int iPoints ){ if( iPoints == m_iPoints )return; m_iPoints = iPoints; onPointsChanged();}
    inline void setRank( int iRank ){ if( iRank == m_iRank )return; m_iRank = iRank; onRankChanged();}
    inline void setSb( int iSb ){ if( iSb == m_iSb )return; m_iSb = iSb; onSbChanged();}
    inline void setWins( int iWins ){ if( iWins == m_iWins )return; m_iWins = iWins; onWinsChanged();}
private:
    int m_iBuchholz;
    int m_iDraws;
    int m_iLoose;
    int m_iPoints;
    int m_iRank;
    int m_iSb;
    int m_iWins;
    Player* m_plr;
};

#endif // RANKING_H
