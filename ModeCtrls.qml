import QtQuick 2.4

import KMode 1.0

ModeCtrlsForm {
    id: modeCtrl
    KMode{
        id: km
    }

    Connections{
        target: btnCreateMode
        onClicked:{
            km.name = editMode.text
            km.balls = sliderBalls.value
            km.sets = sliderSets.value

            switch( modelModes.get( comboModes.currentIndex ).mode ){
            case KMode.MODE_MDYP:
                km.mode = modelModes.get( comboModes.currentIndex ).mode
                break
            case KMode.MODE_PRELIM:
                km.mode = KMode.MODE_PRELIM
                if( checkDouble.checked ){
                    km.mode |= KMode.MODE_DOUBLE
                    if( checkDyp.checked )km.mode |= KMode.MODE_DYP
                }
                break
            }
            kitt.newMode( km )
        }
    }

    Connections{
        target: btnLoad
        onClicked: fileDlg.open()
    }

    Connections{
        target: checkDouble
        onCheckedChanged: if( !checkDouble.checked ) checkDyp.checked = false
    }

    Connections{
        target: checkDyp
        onCheckedChanged: if( checkDyp.checked ) checkDouble.checked = true
    }

    Connections{
        target: comboModes
        onCurrentIndexChanged:{
            switch( modelModes.get( comboModes.currentIndex ).mode ){
            case KMode.MODE_MDYP:
                modeCtrl.state = "mdyp"
                break
            case KMode.MODE_PRELIM:
                modeCtrl.state = "*"
                break
            }
        }
    }

    states:[
        State{
            name: "mdyp"
            PropertyChanges { target: checkDouble; checked: true; enabled: false }
            PropertyChanges { target: checkDyp; checked: true; enabled: false }
        }
    ]

    state: "mdyp"
}

