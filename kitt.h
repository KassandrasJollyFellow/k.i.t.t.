#ifndef KITT_H
#define KITT_H

#define L_OK "OK"

#include <QDataStream>
#include <QFile>
#include <QObject>
#include <QStringList>
#include <QUrl>

#include "logger.h"
#include "prelim.h"

/*!The KITT class is the main class of project K.I.T.T., managing the model part of the model / view architecture.
 *
 * This class is registered in the QML framework as type _KITT_ version 1.0
 */
class KITT : public QObject
{
    Q_OBJECT
public:
    explicit KITT(QObject *parent = 0);
    ~KITT();

signals: 
    void modeAdded( Mode* mode ); /*!< Mode _mode_ was added to this KITT object. */
    void modeDeleted( QString strName ); /*!< Mode object with the name _strName_ was deleted and removed from this KITT object.*/
    void plrAdded( Player* plr );/*!< Player object _plr_ was added to this KITT object.*/
    void plrAddedToMode( Player* plr, Mode* mode );/*!< Player object _plr_ was added to Mode object _mode_.*/
    void plrKilled( QString strName );/*!< Player object with the name _strName_ was deleted and removed from this KITT object.*/
    void plrRemovedFromMode( Player* plr, Mode* mode );/*!< Player object _plr_ was removed from Mode object _mode_.*/
    void tableAdded( QString strTable );/*!< Table with name _strTable_ was added to this KITT object.*/
    void tableKilled( QString strTable );/*!< Table with name _strTable_ was removed from this KITT object.*/

public slots:    
    inline int getModeCount(){ return m_lMode.count();} /*!< Returns number of Mode objects stored in this KITT object.*/
    inline int getPlrCount(){ return m_lPlr.count();}/*!< Returns number of Player objects stored in this KITT object.*/
    inline int getTableCount(){ return m_slTables.count();}/*!< Returns the number of tables stored in thos KITT object.*/
    /*!
     * \brief Returns Mode object with index _index_.
     * \param index Index of the Mode object stored in KITT::m_lMode.
     * \return If _index_ is < 0 or _index_ is >= KITT::m_lMode.count(), _NULL_ is returned.
     * Otherwise a pointer to the Mode object is returned.
     */
    inline Mode* getMode( int index ){ if( index < 0 || index >= m_lMode.count()) return NULL; return m_lMode.at( index );}
    /*!
     * \brief Returns Mode object with name _strName_.
     * \param strName Name of the Mode object stored in KITT::m_lMode.
     * \return If a Mode object with name _strName_ was found, a pointer to this object.
     * Otherwise _NULL_.
     */
    inline Mode* getMode( QString strName ){ foreach( Mode* pm, m_lMode )if( pm->name() == strName )return pm; return NULL;}
    /*!
     * \brief Returns Player object with the index _index_.
     * \param index Index of the Player object stored in KITT::m_lPlr.
     * \return If _index_ is < 0 or _index_ is >= KITT::m_lPlr.count(), _NULL_ is returned.
     * Otherwise a pointer to the Player object.
     */
    inline Player* getPlr( int index ){ if( index < 0 || index >= m_lPlr.count()) return NULL; return m_lPlr.at( index );}
    /*!
     * \brief Returns Player object with the name _strName_.
     * \param strName Name of the Player object stored in KITT::m_lPlr.
     * \return If a Player object with name _strName_ exists, a pointer to this object.
     * Otherwise _NULL_.
     */
    inline Player* getPlr( QString strName ){ foreach( Player* plr, m_lPlr )if( plr->name() == strName )return plr; return NULL;}
    /*!
     * \brief Returns the name of the table with index _index_.
     * \param index Index of the table stored in this KITT object.
     * \return If _index_ is < 0 or _index_ is >= KITT::m_slTables.count(), _NULL_ is returned.
     * Otherwise the name of the table with index _index_.
     */
    inline QString getTable( int index ){ if( index < 0 || index >= m_slTables.count()) return NULL;  return m_slTables.at( index );}

    Player* newPlr( QString strName );

    void deleteMode( Mode* pm );
    void deletePlr( QString strName );
    void deleteTable( QString strTable );
    void loadPlrBase(QString strFile);
    void loadTableBase( QString strFile );
    void newMode(Mode *pm, QUrl urlFile = QUrl( "" ));
    void newTable( QString strTable );
    void savePlrBase( QString strFile );
    void saveTableBase( QString strFile );

private slots:
    void loadPlr( Mode* mode, QString strPlr, QString strPartner );
private:
    unsigned int getEventMode( QUrl url );

    Logger m_log;                           /*!< Logs any activity of this KITT object.*/
    QList< Mode* > m_lMode;       /*!< Stores all Mode objects belonging to this KITT object.*/
    QList< Player* > m_lPlr;           /*!< Stores all Player objects belonging to this KITT object.*/
    QStringList m_slTables;            /*!< Stores all table names belonging to this KITT object.*/
};

#endif // KITT_H
