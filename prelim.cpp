#include "prelim.h"

Prelim::Prelim(QObject *parent)
{
    m_iRound = 1;

    setParent( parent );
}

Prelim::~Prelim()
{
    while( m_lRound.count()){
        delete m_lRound.last();
        m_lRound.pop_back();
    }
}

bool Prelim::assureFeelgood(){
    if( m_lRound.count() < 2 )return true;

    int i, ii;
    Match* pmTst, *pmCur;
    Player* plr, *plrTst;
    Round* prLast = m_lRound.at( m_lRound.count() - 2 );
    Round* prCur = m_lRound.at( m_lRound.count() - 1 );

    foreach( unsigned int id, prCur->lMatchIDs ){
        pmCur = getMatch( id );
        foreach( unsigned int idTst, prLast->lMatchIDs ){
            pmTst = getMatch( idTst );
            for( i = 0; i <= 2; i+= 2 ){
                plr = pmCur->getPlr( i );
                for( ii = 0; ii < 4; ii++ ){
                    if( plr == pmTst->getPlr( ii )){
                        //test for same buddy
                        switch( ii ){
                        case 0:{
                            plrTst = pmTst->getPlr( 1 );
                            break;}
                        case 1:{
                            plrTst = pmTst->getPlr( 0 );
                            break;}
                        case 2:{
                            plrTst = pmTst->getPlr( 3 );
                            break;}
                        case 3:{
                            plrTst = pmTst->getPlr( 2 );
                            break;}
                        }
                        if( plrTst && *plrTst == pmCur->getPlr( i + 1 ))return false; // same buddy
                    }
                }
            }
        }
    }

    return true;
}

Ranks* Prelim::getRanks(){
    int i;
    Player* plr;
    Ranking* pr;
    Ranking* prBest;

    m_ranks.clear();

    foreach( Player* plr, m_lPlr )m_ranks.addRanking( plr );

    foreach( Match* pm, m_lMatch ){
        if( pm->getSetCount()){
            if( pm->getSet( 0 ) < float( m_iBalls )/ 2 )
                    for( i = 0; i < 2; i++ ){
                        if( pm->getPlr( i ))m_ranks.find( pm->getPlr( i )->name())->setWins(
                                    m_ranks.find( pm->getPlr( i )->name())->wins() + 1 );
                        if( pm->getPlr( i + 2 ))m_ranks.find( pm->getPlr( i + 2 )->name())->setLoose(
                                    m_ranks.find( pm->getPlr( i + 2 )->name())->loose() + 1 );
                    }
            else{
                if( pm->getSet( 0 ) > m_iBalls / 2 ){
                    for( i = 0; i < 2; i++ ){
                        if( pm->getPlr( i ))m_ranks.find( pm->getPlr( i )->name())->setLoose(
                                    m_ranks.find( pm->getPlr( i )->name())->loose() + 1 );
                        if( pm->getPlr( i + 2 ))m_ranks.find( pm->getPlr( i + 2 )->name())->setWins(
                                    m_ranks.find( pm->getPlr( i + 2 )->name())->wins() + 1 );
                    }
                }else
                    for( i = 0; i < 4; i++ )if( pm->getPlr( i ))m_ranks.find( pm->getPlr( i )->name())->setDraws(
                                m_ranks.find( pm->getPlr( i )->name())->draws() + 1 );
            }
        }
    }

    for( i = 0; i < m_ranks.count(); i++ ){
        pr = m_ranks[ i ];
        pr->setPoints( pr->wins() * 2 + pr->draws());
    }

    foreach( Match* pm, m_lMatch){
        for( i = 0; i < 4; i++ ){
            if( pm->getPlr( i )){
                plr = pm->getPlr( i );
                switch( i ){
                case 0:
                case 1:{
                    if( pm->getPlr( 2 ))m_ranks.find( plr->name())->setBuchholz( m_ranks.find( plr->name())->buchholz() +
                                                                                 m_ranks.find( pm->getPlr( 2 )->name())->points());
                    if( pm->getPlr( 3 ))m_ranks.find( plr->name())->setBuchholz( m_ranks.find( plr->name())->buchholz() +
                                                                                 m_ranks.find( pm->getPlr( 3 )->name())->points());
                    break;}
                case 2:
                case 3:{
                    if( pm->getPlr( 0 ))m_ranks.find( plr->name())->setBuchholz( m_ranks.find( plr->name())->buchholz() +
                                                                                 m_ranks.find( pm->getPlr( 0 )->name())->points());
                    if( pm->getPlr( 1 ))m_ranks.find( plr->name())->setBuchholz( m_ranks.find( plr->name())->buchholz() +
                                                                                 m_ranks.find( pm->getPlr( 1 )->name())->points());
                    break;}
                }
                if( pm->getSetCount()){
                    if( pm->getSet( 0 ) < ( float )m_iBalls / 2 ){
                        if( i == 0 || i == 1 ){
                            if( pm->getPlr( 2 ))m_ranks.find( plr->name())->setSb( m_ranks.find( plr->name())->sb() +
                                                                                   m_ranks.find( pm->getPlr( 2 )->name())->points());
                            if( pm->getPlr( 3 )){m_ranks.find( plr->name())->setSb( m_ranks.find( plr->name())->sb() +
                                                                                   m_ranks.find( pm->getPlr( 3 )->name())->points());}
                    }}else if( pm->getSet( 0 ) > ( float ) m_iBalls / 2){
                        if( i == 2 || i == 3 ){
                            if( pm->getPlr( 0 ))m_ranks.find( plr->name())->setSb( m_ranks.find( plr->name())->sb() +
                                                                                   m_ranks.find( pm->getPlr( 0 )->name())->points());
                            if( pm->getPlr( 1 )){m_ranks.find( plr->name())->setSb( m_ranks.find( plr->name())->sb() +
                                                                                   m_ranks.find( pm->getPlr( 1 )->name())->points());}
                        }
                    }else if( pm->getSet( 0 ) == ( float )m_iBalls / 2 ){
                        if( i < 2 ){
                            if( pm->getPlr( 2 ))m_ranks.find( plr->name())->setSb( m_ranks.find( plr->name())->sb() +
                                                                                   m_ranks.find( pm->getPlr( 2 )->name())->points());
                            if( pm->getPlr( 3 ))m_ranks.find( plr->name())->setSb( m_ranks.find( plr->name())->sb() +
                                                                                   m_ranks.find( pm->getPlr( 3 )->name())->points());
                        }else{
                            if( pm->getPlr( 0 ))m_ranks.find( plr->name())->setSb( m_ranks.find( plr->name())->sb() +
                                                                                   m_ranks.find( pm->getPlr( 0 )->name())->points());
                            if( pm->getPlr( 1 ))m_ranks.find( plr->name())->setSb( m_ranks.find( plr->name())->sb() +
                                                                                   m_ranks.find( pm->getPlr( 1 )->name())->points());
                        }
                    }
                }
            }
        }
    }

    prBest = m_ranks.get( 0 );

    for( int ii = 0; ii < m_ranks.count(); ii++ ){
        for( i = 0; i < m_ranks.count(); i++ ){
            pr = m_ranks.get( i );
            if( prBest->rank())prBest = pr;
            else{
                if( !pr->rank()){
                    if( pr->points() > prBest->points())prBest = pr;
                    else
                        if( pr->points() == prBest->points() && pr->buchholz() > prBest->buchholz()) prBest = pr;
                        else
                            if( pr->points() == prBest->points() && pr->buchholz() == prBest->buchholz() && pr->sb() > prBest->sb()) prBest = pr;
                }
            }
        }
        prBest->setRank( ii + 1 );
    }

    std::sort( m_ranks.begin(), m_ranks.end(), less );

    return &m_ranks;
}

void Prelim::matchDypPlr(){
    Player *plr, *partner;
    QList< Player* > lplr = m_lPlr;

    for( int iCur = 0; iCur < lplr.count(); iCur++ )
        if( !lplr.at( iCur )->active()){
            lplr.removeAt( iCur );
            iCur--;
        }

    if( lplr.count() % 2 )
        lplr.removeOne( lplr.at( rand() % lplr.count()) );

    while( lplr.count()){
        plr = lplr.at( rand() % lplr.count());
        lplr.removeOne( plr );
        partner = lplr.at( rand() % lplr.count());
        lplr.removeOne( partner );

        plr->setPartner( name(), partner );
        partner->setPartner( name(), plr );
    }
}

void Prelim::newRound(){
    int iCur;
    Match match;
    Player* plrCur;
    QList< Player* > lplr = m_lPlr;
    Round* pRound = new Round;
    static int iFeelgoodCount = 0;

    srand( clock());

    match.setBalls( balls());
    match.setSets(  sets());

    pRound->iRound = m_iRound;

    for( iCur = 0; iCur < lplr.count(); iCur++ ){
        if( !lplr.at( iCur )->active()){
            lplr.removeAt( iCur );
            iCur--;
        }
    }

    if( m_mode == ( MODE_PRELIM | MODE_DOUBLE | MODE_DYP )){
        matchDypPlr();
        m_mode -= MODE_DYP;
    }

    if( m_mode & MODE_DOUBLE && !( m_mode & MODE_MDYP ))
        foreach( Player*plr, m_lPlr )
            if( !plr->partner(( name()))){
                lplr.append( plr );
                break;
            }

    while( lplr.count() % (( m_mode & MODE_DOUBLE ) ? 4 : 2  )){
        plrCur = leastByes( &lplr );
        lplr.removeOne( plrCur );
        plrCur->setByes( plrCur->byes() + 1 );

        if( plrCur->partner( name())){
            plrCur = plrCur->partner( name());
            lplr.removeOne( plrCur );
            plrCur->setByes( plrCur->byes() + 1 );
        }else lplr.removeOne( plrCur );
    }

    while( lplr.count() >= (( m_mode & MODE_DOUBLE ) ? 4 : 2 )){
        switch( m_mode ){
        case MODE_MDYP:
        case MODE_MDYP | MODE_FEELGOOD:{
            for( iCur = 0; iCur < 4; iCur++ ){
                plrCur = lplr.at( rand() % lplr.count());
                match.setPlr( iCur, plrCur );
                lplr.removeOne( plrCur );
            }

            addMatch( &match );
            pRound->lMatchIDs.append( getMatchAtIndex( getMatchCount() - 1 )->id());
            break;
        }
        case MODE_PRELIM:
        case MODE_PRELIM | MODE_FEELGOOD:{
            for( iCur = 0; iCur < 4; iCur+=2  ){
                plrCur = lplr.at( rand() % lplr.count());
                match.setPlr( iCur, plrCur );
                lplr.removeOne( plrCur );
            }
            addMatch( &match );
            pRound->lMatchIDs.append( getMatchAtIndex( getMatchCount() - 1 )->id());
            break;
        }
        case ( MODE_DOUBLE | MODE_PRELIM ):
        case ( MODE_DOUBLE | MODE_PRELIM | MODE_FEELGOOD ):{
            for( iCur = 0; iCur < 4; iCur+= 2 ){
                plrCur = lplr.at( rand() % lplr.count());
                match.setPlr( iCur, plrCur );
                lplr.removeOne( plrCur );

                if( !plrCur->partner( name())){
                    lplr.removeOne( plrCur );
                    match.setPlr( iCur + 1, NULL );                    
                }
                else{
                    match.setPlr( iCur + 1, plrCur->partner( name()));
                    lplr.removeOne( plrCur->partner( name()));
                }
            }
            addMatch( &match );
            pRound->lMatchIDs.append( getMatchAtIndex( getMatchCount() - 1 )->id());
            break;
        }
        default:
            //TODO: error output
            ;//return;
        }
    }

    m_lRound.append( pRound );

    if( m_mode & MODE_FEELGOOD ){
        if( !assureFeelgood() && iFeelgoodCount <= 20){
            foreach( unsigned int id, pRound->lMatchIDs ) removeMatch( id );
            delete pRound;

            m_lRound.pop_back();

            iFeelgoodCount++;
            newRound();
            return;
        }else iFeelgoodCount = 0;
    }

    m_iRound++;
}

