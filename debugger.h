#ifndef DEBUGGER_H
#define DEBUGGER_H

#include <QObject>
#include <QThread>

#include "remotekitt.h"

/*!The Debugger class creates a RemoteKITT object, transfers it to another thread and
 * communicates via Debugger::msgIn and Debugger::msgOut with it.
 *
 * This class is registered in the QML framework as type _KDbg_ version 1.0.
 */
class Debugger : public QObject
{
    Q_OBJECT
public:
    explicit Debugger(QObject *parent = 0);
    ~Debugger();

signals:
    void log( const QVariant &obj ); /*!< Emitted, when Debugger::msgIn receives a RemoteKITT::RKM_LOG signal.*/
    void msgOut( const int &msg, const QVariant &var );/*!< Forwards unknown messages received in Debugger::msgIn.*/
    void progress( QVariant varProgress );/*!< Forwards RemoteKITT::RKM_PROGRESS signals received in Debugger::msgIn.*/
public slots:
    void msgIn( const int &msg, const QVariant &var );
private:
    QThread m_thread; /*!< Thread the RemoteKITT object will be transfered to.*/
};

#endif // DEBUGGER_H
