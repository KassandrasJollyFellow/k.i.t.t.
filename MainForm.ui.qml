import QtQuick 2.4
import QtQuick.Controls 1.3
import QtQuick.Layouts 1.1

import "qrc:/Debug"
import "qrc:/Event"
import "qrc:/Mode"
import "qrc:/Player"
import "qrc:/Ranking"

Item {
    ColumnLayout{
        anchors.fill: parent
        TitleBar{}
        TabView{
            id: tabMain
            Layout.fillWidth: true
            Layout.fillHeight: true
            Tab{
                title: "Spieler"
                active: true
                Player{}
            }
            Tab{
                title: "Modus"
                active: true
                Mode{}
            }
            Tab{
                title: "Spiele"
                active: true
                Tourney{}
            }

            Tab{
                title: "Tabelle"
                active: true
                Ranking{}
            }
        }
    }

    Rectangle {
        id: rectangle1
        color: "#000000"
        z: -1
        border.width: 0
        anchors.fill: parent
    }

    property alias tabMain: tabMain
}
