#include <QApplication>
#include <QQmlApplicationEngine>
#include <QtQml>
#include <QFont>

#include "debugger.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    QQmlApplicationEngine engine;

    qmlRegisterType< Debugger >( "KDbg", 1, 0, "KDbg" );
    qmlRegisterType< KITT >( "KITT", 1, 0, "KITT" );
    qmlRegisterType< Logger >( "Logger", 1, 0, "Logger" );
    qmlRegisterType< Match >( "KMatch", 1, 0, "Match" );
    qmlRegisterType< Mode >( "KMode", 1, 0, "KMode" );
    qmlRegisterType< Player >( "KPlr", 1, 0, "KPlr" );
    qmlRegisterType< Ranking >( "KRanking", 1, 0, "KRanking" );
    qmlRegisterType< Ranks >( "KRanks", 1, 0, "KRanks" );    
    qmlRegisterType< RemoteKITT >( "RemoteKITT", 1, 0, "RemoteKITT" );
    qmlRegisterType< TeamUp >( "TeamUp", 1, 0, "TeamUp" );

    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    return app.exec();
}
