import QtQuick 2.4

import KDbg 1.0
import RemoteKITT 1.0

DebugForm {
    KDbg{
        id: kd
        onLog: txtDbg.append( obj.toString())
        onProgress: progress.value = varProgress.toFloat() * 100
    }

    Connections{
        target: btnDump
        onClicked:{
            txtDbg.append( kitt.getModeCount())
            for( var i = 0; i < kitt.getModeCount(); i++ ){
                txtDbg.append( kitt.getMode( i ).name )
                for( var ii = 0; ii < kitt.getMode( i ).getPlrCount(); ii++ ) txtDbg.append( kitt.getMode( i ).getPlr( ii ).name )
            }
        }
    }

    Connections{
        target: btnStart
        onClicked:{
            kd.msgIn( RemoteKITT.RKM_DBG_SETBALL, sliderBall.value )
            kd.msgIn( RemoteKITT.RKM_DBG_SETPLR, sliderPlr.value )
            kd.msgIn( RemoteKITT.RKM_DBG_SETROUND, sliderRound.value )
            kd.msgIn( RemoteKITT.RKM_DBG_SETSET, sliderSet.value )
            kd.msgIn( RemoteKITT.RKM_DBG_SETTABLE, sliderTable.value )
            kd.msgIn( RemoteKITT.RKM_DBG, 0 )
        }
    }
}

