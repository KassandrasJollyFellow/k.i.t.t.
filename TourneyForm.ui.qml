import QtQuick 2.4
import QtQuick.Controls 1.2

Item {
    anchors.fill: parent

    Rectangle{
        anchors.fill: parent
        color: "#000000"
    }

    TabView{
        id: tabEvents;
        anchors.fill: parent
    }

    property alias tabEvents: tabEvents
}

