#ifndef PRELIM_H
#define PRELIM_H

#include <QList>

#include <algorithm>
#include <stdlib.h>
#include <time.h>

#include "mode.h"

struct Round{
    unsigned int iRound;
    QList < unsigned int > lMatchIDs;
};

class Prelim : public Mode
{
public: 
    explicit Prelim(QObject *parent = 0);
    ~Prelim();
public slots:
    static inline bool less( Ranking* pr1, Ranking* pr2 ){ if( !pr1 || !pr2 )return false; if( pr1->rank() > pr2->rank()) return false; return true; }
    Ranks* getRanks();
    void newRound();
private:
    bool assureFeelgood();
    void matchDypPlr();

    QList < Round* > m_lRound;
};

#endif // PRELIM_H
