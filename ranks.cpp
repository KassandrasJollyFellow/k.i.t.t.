#include "ranks.h"

Ranks::Ranks(QObject *parent) : QObject(parent)
{    
}

Ranks::~Ranks()
{

}

int Ranks::addRanking( Player* plr ){
    if( !plr )return -1;

    Ranking* r = new Ranking( this );

    r->setPlayer( plr );

    m_lRankings.append( r );

    rankingAdded( r );

    return m_lRankings.count() - 1;
}

void Ranks::clear(){
    foreach( Ranking* pr, m_lRankings ){
        rankingRemoved( pr->player());
        delete pr;
    }

    m_lRankings.clear();
}
