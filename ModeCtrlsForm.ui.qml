import QtQuick 2.4
import QtQuick.Controls 1.2
import QtQuick.Layouts 1.0
import QtQml.Models 2.1

import KMode 1.0

Item {
    height: 200
    width: 800
    Rectangle {
        id: rectangle1
        color: "#000000"
        border.width: 0
        anchors.fill: parent
    }

    RowLayout {
        id: rowMode
        height: 50
        spacing: 10
        anchors{ right: parent.right; rightMargin: 10; left: parent.left; leftMargin: 10; top: parent.top; topMargin: 10 }

        TextField {
            id: editMode
            Layout.minimumWidth: 150
            placeholderText: qsTr("Name des Turniers")
        }

        Button{
            id: btnLoad
            text: "Laden"
        }

        ComboBox {
            id: comboModes
            Layout.fillWidth: true
            model: ListModel{
                id: modelModes
                ListElement{ text: "Monster - D.Y.P."; mode: KMode.MODE_MDYP }
                ListElement{ text: "Vorrunde"; mode: KMode.MODE_PRELIM }
            }
        }

        CheckBox {
            id: checkDouble
            text: qsTr("<font color=\"white\">Doppel</font>")
        }

        CheckBox {
            id: checkDyp
            text: qsTr("<font color=\"white\">D.Y.P.</font>")
        }
    }

    RowLayout {
        id: rowBalls
        height: 50
        spacing: 10
        anchors.right: parent.right
        anchors.rightMargin: 10
        anchors.left: parent.left
        anchors.leftMargin: 10
        anchors.top: rowMode.bottom
        anchors.topMargin: 0

        Label {
            id: label1
            color: "#ffffff"
            text: qsTr("Bälle:")
            anchors.left: parent.left
            anchors.leftMargin: 0
        }

        Label {
            id: labelBalls
            color: "#ffffff"
            text: sliderBalls.value
        }

        Slider {
            id: sliderBalls
            Layout.fillWidth:  true
            maximumValue: 19
            minimumValue: 10
            stepSize: 1
            value: 10
        }
    }

    RowLayout {
        id: rowSets
        height: 50
        anchors.top: rowBalls.bottom
        anchors.topMargin: 0
        anchors.left: parent.left
        anchors.leftMargin: 10
        anchors.rightMargin: 10
        spacing: 10
        anchors.right: parent.right
        Label {
            id: label2
            color: "#ffffff"
            text: qsTr("Sätze:")
            anchors.left: parent.left
            anchors.leftMargin: 0
        }

        Label {
            id: labelSets
            color: "#ffffff"
            text: sliderSets.value
        }

        Slider {
            id: sliderSets
            Layout.fillWidth: true
            maximumValue: 9
            value: 1
            minimumValue: 1
            stepSize: 1
        }
    }

    Button {
        id: btnCreateMode
        text: qsTr("Turnier erstellen")
        anchors.right: parent.right
        anchors.rightMargin: 10
        anchors.left: parent.left
        anchors.leftMargin: 10
        anchors.top: rowSets.bottom
        anchors.topMargin: 0
    }           

    property alias btnCreateMode: btnCreateMode
    property alias btnLoad: btnLoad
    property alias checkDouble: checkDouble
    property alias checkDyp: checkDyp
    property alias comboModes: comboModes
    property alias editMode: editMode
    property alias modelModes: modelModes
    property alias sliderBalls: sliderBalls
    property alias sliderSets: sliderSets
}

