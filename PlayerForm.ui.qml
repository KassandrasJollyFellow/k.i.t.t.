import QtQuick 2.4
import QtQuick.Controls 1.2

import "qrc:/Player"
import QtQuick.Layouts 1.0

Item {
    id: plrForm
    width: 800
    height: 600

    Rectangle {
        id: rectangle1
        width: 0
        height: 0
        color: "#000000"
        border.width: 0
        anchors.fill: parent
    }

    ColumnLayout {
        id: columnMain
        anchors{
        fill: parent
        leftMargin: 10
        rightMargin: 10
        topMargin: 10
        bottomMargin: 10
        }
        spacing: 10
        GridLayout {
            columns: 5
            id: gridCtrls
            rowSpacing: 10
            columnSpacing: 10

            TextField {
                id: editPlr
                Layout.fillWidth: true
                placeholderText: qsTr("Name des Spielers")
            }

            Button{
                id: btnCreatePlr
                text: qsTr("erstellen")
            }

            Button {
                id: btnAddPlr
                Layout.fillWidth: true
                text: qsTr("zu Modus hinzufügen / herausnehmen")
            }

            Button {
                id: btnKillPlr
                text: qsTr("löschen*")
            }

            CheckBox{
                id: checkSure
                text: "<font color=\"white\">* sicher</font>"
            }

            Label {
                id: labelPartner
                color: "#ffffff"
                text: qsTr("Partner:")
            }

            ComboBox {
                editable: true
                id: comboPartner
                model: tablePlrModel
            }

            Button{
                id: btnAddModeToAll
                text: qsTr( "Modus zu allen hinzufügen*" )
                visible: false
            }
        }

        TableView{
            id: tableModes
            Layout.fillWidth: true
            model: ListModel{ id: modelModes }
            TableViewColumn{
                title: "Modus"
                role: "mode"
            }
        }

        TableView{
            id: tablePlr
            itemDelegate: TPlrDelegate{}
            Keys.forwardTo: [ Qt.Key_Space ]
            Layout.fillHeight: true
            Layout.fillWidth: true
            model: ListModel{
                id: tablePlrModel

            }

            TableViewColumn{
                title: "Spieler"
                role: "name"
            }
            TableViewColumn{
                title: "Modi"
                role: "modes"
            }
        }
    }

    property alias btnAddModeToAll: btnAddModeToAll
    property alias btnAddPlr: btnAddPlr
    property alias btnCreatePlr: btnCreatePlr
    property alias btnKillPlr: btnKillPlr
    property alias checkSure: checkSure
    property alias comboPartner: comboPartner
    property alias editPlr: editPlr
    property alias labelPartner: labelPartner
    property alias modelModes: modelModes
    property alias tableModes: tableModes
    property alias tablePlr: tablePlr
    property alias tablePlrModel: tablePlrModel
}

