import QtQuick.Layouts 1.1
import QtQuick 2.0
import QtQuick.Controls 1.2

import KMode 1.0

ColumnLayout{
    spacing: 30
    Layout.fillWidth: true
    RowLayout{
        Layout.fillWidth: true
        spacing: 10
        Button{
            implicitHeight: 60
            Layout.fillWidth: true
            text: "Runde auslosen"
            onClicked: km.newRound()
        }
        CheckBox{
            id: checkFeelgood
            text: "<font color=\"white\">Feelgood</font>"
            onClicked:{
                if( km.mode & KMode.MODE_FEELGOOD ) km.setMode( km.mode - KMode.MODE_FEELGOOD )
                else km.setMode( km.mode + KMode.MODE_FEELGOOD )
            }
        }

        Button{
            text: "Runde löschen*"            
            onClicked:{
                if( !checkSure.checked )return

                var round = modelGames.get( tableGames.currentRow ).round

                for( var i = 0; i < modelGames.count; i++ )
                    if( modelGames.get( i ).round === round ){
                        km.removeMatch( modelGames.get( i ).id );
                        i--;
                    }

                checkSure.checked = false
            }
        }

        Button{
            text: "Modus löschen*"
            onClicked: if( checkSure.checked )kitt.deleteMode( km )
        }

        CheckBox{
            id: checkSure
            text: "<font color=\"white\">* sicher</font>"
        }
    }
    Column{
        spacing: 30
        anchors.left: parent.left
        anchors.right: parent.right        
        Button{
            id: btnSetRes
            anchors.left: parent.left
            anchors.right: parent.right
            text: getGoalsFirst() + " : " + getGoalsSecond() + " Tore eintragen" +
                  ( km.getMatch( modelGames.get( tableGames.currentRow ).id ).getSetCount() ? "*" : "" )
            implicitHeight: 60
            onClicked:{
                if(( text.indexOf( "*" )  !== -1 ) && checkSure.checked === false )return
                km.removeSet( modelGames.get( tableGames.currentRow ).id, 0 )
                km.addSet( modelGames.get( tableGames.currentRow ).id, sliderBalls.value )
                km.freeTable( modelGames.get( tableGames.currentRow ).table )
                modelGames.setProperty( tableGames.currentRow, "table", "" )
                km.testForTablesToPlay()
                checkSure.checked = false
            }
        }
        Slider{
            id: sliderBalls
            anchors.left: parent.left
            anchors.right: parent.right
            minimumValue: 0
            maximumValue: km.balls
            stepSize: 1
        }
    }

    function getGoalsFirst(){
        var v = sliderBalls.value
        if( v === sliderBalls.maximumValue / 2 ) return v
        if( v > sliderBalls.maximumValue / 2 ) return sliderBalls.maximumValue - sliderBalls.value
        return ( sliderBalls.maximumValue - sliderBalls.maximumValue % 2 ) / 2 + 1
    }

    function getGoalsSecond(){
        var v = sliderBalls.value

        if( v === sliderBalls.maximumValue / 2 ) return v
        if( v < sliderBalls.maximumValue / 2 ) return sliderBalls.value
        return ( sliderBalls.maximumValue - sliderBalls.maximumValue % 2 ) / 2 + 1

    }
    property alias btnSetRes: btnSetRes
    property alias sliderBalls: sliderBalls
}
